import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.Month;
import com.epam.gomel.homework.Mood;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static com.epam.gomel.homework.Month.*;
import static com.epam.gomel.homework.Mood.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.mock;

public class TestsForGirl extends BaseTest {

    private Boy boy;
    private Girl girl;

    @BeforeClass
    public void setGirl() {
        boy = new Boy(AUGUST, 100_001);
        girl = new Girl(false, true, boy);
    }

    public static Matcher<Girl> pretty() {
        return new FeatureMatcher<Girl, Boolean>(equalTo(true), "Girl should be pretty", "Pretty -") {
            @Override
            protected Boolean featureValueOf(Girl girl) {
                return girl.isPretty();
            }
        };
    }

    public static Matcher<Girl> rich() {
        return new FeatureMatcher<Girl, Boolean>(equalTo(true), "Girl should be pretty", "Pretty -") {
            @Override
            protected Boolean featureValueOf(Girl girl) {
                return girl.isBoyfriendRich();
            }
        };
    }

    @Test(description = "Test for girl setter with hamcrest matcher", dependsOnMethods = "mockedBoy", priority = 2)
    public void testForGirlSetterWithHamcrest() {
        girl = new Girl();
        girl.setPretty(true);
        assertThat(girl, pretty());
    }

    @Test(description = "Test for girl setter with mocked boy")
    public void mockedBoy() {
        Boy mockedBoy = mock(Boy.class);
        girl = new Girl();
        mockedBoy.setGirlFriend(girl);
        Mockito.verify(mockedBoy).setGirlFriend(girl);
    }

    @Test(description = "Test for method isSlimFriendBecameFat for scary girl and fat friend")
    @Parameters({"isPretty", "isSlimFriendGotAFewKilos"})
    public void testIsSlimFriendBecameFatForScaryGirl(boolean isPretty, boolean isSlimFriendGotAFewKilos) {
        girl = new Girl(isPretty, isSlimFriendGotAFewKilos);
        Assert.assertEquals(girl.isSlimFriendBecameFat(), true, "Invalid obesity");
    }

    @DataProvider(name = "Data for SlimFriendBecameFat")
    public Object[][] dataSlimFriendBecameFatTest() {
        return new Object[][]{
                {false, false, false},
                {true, true, false},
                {true, false, false}
        };
    }

    @Test(dataProvider = "Data for SlimFriendBecameFat", description = "Test for method isSlimFriendBecameFat", priority = 1)
    public void testIsSlimFriendBecameFat(boolean isPretty, boolean isSlimFriendGotAFewKilos, boolean expectedResult) {
        girl = new Girl(isPretty, isSlimFriendGotAFewKilos);
        Assert.assertEquals(girl.isSlimFriendBecameFat(), expectedResult, "Invalid obesity");
    }

    @DataProvider(name = "Data for NewShoes")
    public Object[][] dataForNewShoesTest() {
        return new Object[][]{
                {true, true, boy = new Boy(OCTOBER, 1_000_000)},
                {false, false, boy = new Boy(OCTOBER, 2_000_000)},
                {true, false, boy = new Boy(OCTOBER, 1_000)}
        };
    }

    @Test(dataProvider = "Data for NewShoes", description = "Test for method isBoyFriendWillBuyNewShoes")
    public void testIsBoyFriendWillBuyNewShoes(boolean isPretty, boolean expectedResult, Boy boy) {
        girl = new Girl(isPretty, true, boy);
        Assert.assertEquals(girl.isBoyFriendWillBuyNewShoes(), expectedResult, "Some troubles with buying new shoes");
    }

    @DataProvider(name = "Data for isBoyfriendRich")
    public Object[][] dataForIsBoyfriendRichTest() {
        return new Object[][]{
                {boy = new Boy(OCTOBER, 1_000_000), true, true},
                {boy = new Boy(OCTOBER, 2_000_000), false, true},
                {boy = new Boy(OCTOBER, 1_000), true, false}
        };
    }

    @Test(description = "Test for method isBoyfriendRich", dataProvider = "Data for isBoyfriendRich", priority = 3)
    public void testIsBoyfriendRich(Boy boy, boolean isPretty, boolean expectedResult) {
        girl = new Girl(isPretty, false, boy);
        Assert.assertEquals(girl.isBoyfriendRich(), expectedResult, "Some troubles with wealth");
    }

    @DataProvider(name = "Data for spendBoyFriendMoney")
    public Object[][] dataForSpendBoyFriendMoneyTest() {
        return new Object[][]{
                {AUGUST, 100, 1_000_000, 999_900},
                {JUNE, 100, 900_000, 900_000},
                {FEBRUARY, 2_100_000, 1_100_000, 1_100_000},
                {MARCH, -100_000, 9_000, 9_000},
                {MAY, 100_000, -1_900_000, -1_900_000}
        };
    }

    @Test(dataProvider = "Data for spendBoyFriendMoney", description = "Test for method spendBoyFriendMoney")
    public void testSpendBoyFriendMoney(Month birthdayMonth, double amountForSpending, double wealth, double expectedWealth) {
        boy = new Boy(birthdayMonth, wealth, girl);
        girl.spendBoyFriendMoney(amountForSpending);
        Assert.assertEquals(expectedWealth, boy.getWealth(), 0.0, "Invalid money transaction");
    }

    @DataProvider(name = "Data for mood")
    public Object[][] dataForMoodTest() {
        return new Object[][]{
                {AUGUST, 2_000_000, true, false, EXCELLENT},
                {JULY, 100_000, true, false, GOOD},
                {JUNE, 1_000_000, false, false, GOOD},
                {DECEMBER, 100, false, true, NEUTRAL},
                {FEBRUARY, 100, false, false, I_HATE_THEM_ALL}
        };
    }

    @Test(dataProvider = "Data for mood", description = "Test for getMood method", priority = 1)
    public void testGetMoodWithExcellentMood(Month birthdayMonth, double wealth, boolean isPretty,
                                             boolean isSlimFriendGotAFewKilos, Mood mood) {
        boy = new Boy(birthdayMonth, wealth);
        girl = new Girl(isPretty, isSlimFriendGotAFewKilos, boy);
        Assert.assertEquals(girl.getMood(), mood, "Invalid mood");
    }
}


