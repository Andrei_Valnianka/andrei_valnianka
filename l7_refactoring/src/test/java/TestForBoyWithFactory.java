import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Month;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestForBoyWithFactory {

    private Boy boy;
    private Month birthdayMonth;
    private double wealth;
    private boolean expectedResult;

    public TestForBoyWithFactory(Month birthdayMonth, double wealth, boolean expectedResult) {
        this.birthdayMonth = birthdayMonth;
        this.wealth = wealth;
        this.expectedResult = expectedResult;
    }

    @Test(description = "Test for method isRich", groups = "boy with factory")
    public void testIsRich() {
        boy = new Boy(birthdayMonth, wealth);
        Assert.assertEquals(boy.isRich(), expectedResult, "Invalid money checking in method isRich");
    }
}
