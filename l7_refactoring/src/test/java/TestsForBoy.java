import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.Month;
import com.epam.gomel.homework.Mood;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static com.epam.gomel.homework.Month.*;
import static com.epam.gomel.homework.Mood.*;

public class TestsForBoy extends BaseTest {

    private Boy boy;
    private Girl girl;
    private Month birthdayMonth = JANUARY;
    private double wealth = 100;

    @DataProvider(name = "Data for isRich")
    public Object[][] dataForIsRichTest() {
        return new Object[][]{
                {SEPTEMBER, 1_000_000, true},
                {OCTOBER, 2_000_000, true},
                {MAY, 1_000, false},
                {JANUARY, -1_000, false}
        };
    }

    @Test(description = "Test for method isRich", dataProvider = "Data for isRich", groups = "tests")
    public void testIsRich(Month birthdayMonth, double wealth, boolean expectedResult) {
        boy = new Boy(birthdayMonth, wealth);
        Assert.assertEquals(boy.isRich(), expectedResult, "Invalid money checking in method isRich");
    }

    @DataProvider(name = "Data for prettyGirl")
    public Object[][] dataForPrettyGirlTest() {
        return new Object[][]{
                {SEPTEMBER, 1_000_000, true, true},
                {OCTOBER, 2_000_000, true, true},
                {MAY, 1_000, false, false},
                {JANUARY, -1_000, false, false}
        };
    }

    @Test(description = "Test for method IsPrettyGirlFriend", dataProvider = "Data for prettyGirl", groups = "tests")
    public void testIsPrettyGirlFriend(Month birthdayMonth, double wealth, boolean isPretty, boolean expectedResult) {
        girl = new Girl(false);
        boy = new Boy(birthdayMonth, wealth, girl);
        Assert.assertEquals(boy.isPrettyGirlFriend(), expectedResult, "Invalid girlfriend");
    }

    @Test(description = "Test for method IsPrettyGirlFriend with no girl", groups = {"tests", "negative"})
    public void testIsPrettyGirlFriendWithNoGirlFriend() {
        girl = null;
        boy = new Boy(birthdayMonth, wealth, girl);
        Assert.assertEquals(boy.isPrettyGirlFriend(), false, "Invalid girlfriend");
    }

    @DataProvider(name = "Data for summerMonth")
    public Object[][] dataForSummerMonthTest() {
        return new Object[][]{
                {SEPTEMBER, false},
                {OCTOBER, false},
                {JUNE, true},
                {JULY, true},
                {AUGUST, true},
                {JANUARY, false}
        };
    }

    @Test(description = "Test for method IsPrettyGirlFriend", dataProvider = "Data for summerMonth", groups = "tests")
    public void testIsSummerMonth(Month birthdayMonth, boolean expectedResult) {
        boy = new Boy(birthdayMonth);
        Assert.assertEquals(boy.isSummerMonth(), expectedResult, "Invalid birthday month");
    }

    @Test(description = "Test for method spendSomeMoney with exception",
            expectedExceptions = RuntimeException.class,
            expectedExceptionsMessageRegExp = "Not enough money! .*",
            groups = {"tests", "negative"})
    public void testSpendSomeMoneyWithException() {
        boy = new Boy(birthdayMonth, wealth, girl);
        double amountForSpending = 2_500_020;
        if (amountForSpending > boy.getWealth()) {
            boy.spendSomeMoney(amountForSpending);
        }
    }

    @DataProvider(name = "Data for spendSomeMoney")
    public Object[][] dataForSpendSomeMoneyTest() {
        return new Object[][]{
                {SEPTEMBER, 1_000_000, 1_500_000},
                {OCTOBER, 2_000_000, 200},
                {JUNE, 1_000, 50},
                {NOVEMBER, 1_000_100, -50},
                {JANUARY, -1_000, 10}
        };
    }

    @Test(description = "Test for method IsPrettyGirlFriend", dataProvider = "Data for spendSomeMoney", groups = "tests")
    public void testSpendSomeMoney(Month birthdayMonth, double wealth, double amountForSpending) {
        boy = new Boy(birthdayMonth, wealth);
        boy.spendSomeMoney(amountForSpending);
        double afterSpending = boy.getWealth();
        Assert.assertEquals(afterSpending, wealth - amountForSpending, 0.0, "Some problem with money transaction");
    }

    @DataProvider(name = "Data for getMood")
    public Object[][] dataForGetMoodTest() {
        return new Object[][]{
                {JULY, 1_001_000, true, EXCELLENT},
                {FEBRUARY, 1_500_000, true, GOOD},
                {AUGUST, 2_000_000, false, NEUTRAL},
                {JULY, 100_000, false, BAD},
                {OCTOBER, 5_000, true, BAD},
                {DECEMBER, 1_000_000, false, BAD},
                {MAY, 1_000, false, HORRIBLE}
        };
    }

    @Test(description = "Test for method IsPrettyGirlFriend", dataProvider = "Data for getMood", groups = "tests")
    public void testGetMood(Month birthdayMonth, double wealth, boolean isPretty, Mood expectedResult) {
        girl = new Girl(isPretty);
        boy = new Boy(birthdayMonth, wealth, girl);
        Assert.assertEquals(boy.getMood(), expectedResult, "Invalid mood");
    }
}
