import com.epam.gomel.homework.Month;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import static com.epam.gomel.homework.Month.*;

public class FactoryTestForBoy {
    @DataProvider(name = "Data for boys creation")
    public Object[][] getBoysData() {
        return new Object[][]{
                {JUNE, 1_000_000, true},
                {JULY, 1_000, false},
                {FEBRUARY, -100, false},
                {AUGUST, 500_000, false},
                {NOVEMBER, 2_000_000, true}
        };
    }

    @Factory(dataProvider = "Data for boys creation")
    public Object[] getBoysTet(Month birthdayMonth, double wealth, boolean expectedResult) {
        return new Object[]{new TestForBoyWithFactory(birthdayMonth, wealth, expectedResult)};
    }


}
