package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LeftToolBarPage {

    public static final int WAIT_TIME_OUT = 5;
    public static final int FRAME_INDEX = 0;

    WebDriver driver;

    @FindBy(xpath = "//a[@data-name='compose']")
    private WebElement writeLetterButton;

    @FindBy(xpath = "//div[@data-id='0']")
    private WebElement inboxLettersButton;

    @FindBy(xpath = "//div[@data-id='500000']")
    private WebElement outboxLettersButton;

    @FindBy(xpath = "//div[@data-id='500002']")
    private WebElement deletedLettersButton;

    @FindBy(xpath = "//div[@data-id='500001']")
    private WebElement draftsLettersButton;

    public LeftToolBarPage(WebDriver driver) {
        this.driver = driver;
    }

    public WriteLetterPage clickOnWriteLetterButton() {
        writeLetterButton.click();
        return new WriteLetterPage(driver);
    }

    public InboxPage clickOnInboxLettersButton() {
        inboxLettersButton.click();
        return new InboxPage(driver);
    }

    public OutboxPage clickOnOutboxLettersButton() {
        outboxLettersButton.click();
        return new OutboxPage(driver);
    }

    public DeletedPage clickOnDeletedLettersButton() {
        deletedLettersButton.click();
        return new DeletedPage(driver);
    }

    public DraftsPage clickOnDraftsLettersButton() {
        draftsLettersButton.click();
        return new DraftsPage(driver);
    }
}
