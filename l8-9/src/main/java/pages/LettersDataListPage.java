package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static pages.LeftToolBarPage.WAIT_TIME_OUT;

public class LettersDataListPage {

    private WebDriver driver;

    public boolean checkMessage(String letterSubject, String letterText) {
        return driver.findElement(By.xpath("//*[@class='b-datalist__item__subj' and text()='" + letterSubject
                + "']")).isDisplayed() && (driver.findElement(By.xpath(
                "//span[@class='b-datalist__item__subj__snippet' and text()='" + letterText + "']")).isDisplayed());
    }

    public boolean checkDeletedMessage(String letterSubject, String letterText) {
        try {
            driver.findElement(By.xpath("//*[@class='b-datalist__item__subj' and text()='" + letterSubject + "']"));
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public void waiterCheckDeletedMessage(String letterSubject) {
        WebDriverWait waitForOne = new WebDriverWait(driver, WAIT_TIME_OUT);
        try {
            waitForOne.until(ExpectedConditions.visibilityOfElementLocated(
                    By.xpath("//*[@class='b-datalist__item__subj' and text()='" + letterSubject + "']")));
        } catch (TimeoutException e) {
            System.out.println(e);
        }
    }

    public boolean checkEmptyMessage(String letterSubject, String letterText) {
        return driver.findElement(By.xpath("//*[@class='b-datalist__item__subj' and text()='<Без темы>']"))
                .isDisplayed() && driver.findElement(By.xpath("//span[@class='b-datalist__item__subj__snippet']"))
                .isDisplayed();
    }

    public void deleteMessage(String letterSubject) {
        driver.findElement(By.xpath("//*[@data-subject='" + letterSubject
                + "']/div[@class='js-item-checkbox b-datalist__item__cbx']"))
                .click();
        driver.findElement(By.xpath("//div[@id='b-toolbar__right']//div[contains(@data-cache-key,'undefined_false') "
                + "and not (contains(@style, 'display: none;'))]//div[@data-name='remove']"))
                .click();
    }

    public void waiterForDeleteMessage(String letterSubject) {
        WebDriverWait waitForOne = new WebDriverWait(driver, WAIT_TIME_OUT);
        try {
            waitForOne.until(ExpectedConditions.visibilityOfElementLocated(
                    By.xpath("//*[@data-subject='" + letterSubject
                            + "']/div[@class='js-item-checkbox b-datalist__item__cbx']")));
        } catch (TimeoutException e) {
            System.out.println(e);
        }
    }
}


