package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static pages.LeftToolBarPage.WAIT_TIME_OUT;

public class InboxPage extends LettersDataListPage {

    private WebDriver driver;

    public InboxPage(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getInboxPageDriver() {
        return driver;
    }

    public void waiterInboxPage() {
        WebDriverWait waitForOne = new WebDriverWait(driver, WAIT_TIME_OUT);
        try {
            waitForOne.until(ExpectedConditions.visibilityOfElementLocated(By.id("PH_user-email")));
        } catch (TimeoutException e) {
            System.out.println(e);
        }
    }

    public WebDriver waiterSendButton() {
        WebDriverWait waitForOne = new WebDriverWait(driver, WAIT_TIME_OUT);
        try {
            waitForOne.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@data-name='compose']")));
        } catch (TimeoutException e) {
            System.out.println(e);
        }
        return driver;
    }

    public String getUserName() {
        String result = "";
        if (driver.findElement(By.id("PH_user-email")).getText() != null) {
            result = driver.findElement(By.id("PH_user-email")).getText();
        }
        return result;
    }
}
