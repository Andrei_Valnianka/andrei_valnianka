package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static pages.LeftToolBarPage.FRAME_INDEX;
import static pages.LeftToolBarPage.WAIT_TIME_OUT;

public class WriteLetterPage {

    WebDriver driver;

    @FindBy(xpath = "//textarea[@data-original-name='To']")
    private WebElement address;

    @FindBy(xpath = "//input[@name='Subject']")
    private WebElement subject;

    @FindBy(xpath = "//body[@id='tinymce']")
    private WebElement text;

    @FindBy(xpath = "//*[@data-name='send']")
    private WebElement sendButton;

    @FindBy(xpath = "//*[@id='b-toolbar__right']/div[3]/div/div[2]/div[2]/div/div[1]/span")
    private WebElement saveDraftButton;

    public WriteLetterPage(WebDriver driver) {
        this.driver = driver;
    }

    public void waiterWriteLetterPage() {
        WebDriverWait waitForOne = new WebDriverWait(driver, WAIT_TIME_OUT);
        try {
            waitForOne.until(ExpectedConditions.visibilityOfElementLocated(By.id("//*[@data-name='send']")));
        } catch (TimeoutException e) {
            System.out.println(e);
        }
    }

    public WebDriver sendMessage(String email, String letterSubject, String letterText) {
        address.sendKeys(email);
        subject.sendKeys(letterSubject);
        driver.switchTo().frame(FRAME_INDEX);
        text.sendKeys(letterText);
        driver.switchTo().defaultContent();
        sendButton.click();
        return driver;
    }

    public boolean checkInvalidEmailAlert() {
        return driver.switchTo().alert().getText().isEmpty();
    }

    public WebDriver createDraftMessage(String email, String letterSubject, String letterText) {
        address.sendKeys(email);
        subject.sendKeys(letterSubject);
        driver.switchTo().frame(FRAME_INDEX);
        text.sendKeys(letterText);
        driver.switchTo().defaultContent();
        saveDraftButton.click();
        return driver;
    }
}

