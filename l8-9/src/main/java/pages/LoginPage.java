package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {

    WebDriver driver;

    @FindBy(xpath = "//*[@id='mailbox__login']")
    private WebElement loginInput;

    @FindBy(xpath = "//*[@id='mailbox__password']")
    private WebElement passwordInput;

    @FindBy(xpath = "//*[@id='mailbox__auth__button']")
    private WebElement submitButton;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    public InboxPage successLogin(String email, String password) {
        loginInput.sendKeys(email);
        passwordInput.sendKeys(password);
        submitButton.click();
        return new InboxPage(driver);
    }
}
