package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.*;

import java.util.Random;

public class DraftMailTest extends BaseTest {
    WebDriver driver;
    final Random random = new Random();

    @DataProvider(name = "Data for DraftMail test")
    public Object[][] dataDraftMailTest() {
        return new Object[][]{
                {"vasil_pupkin2018@mail.ru", "ekscentrisitet2018", "Invitation" + String.valueOf(random.nextInt()),
                "Welcome to the hotel California" + String.valueOf(random.nextInt())}
        };
    }

    @Test(dataProvider = "Data for DraftMail test", groups = "tests")
    public void properlySendTest(String email, String password, String letterSubject, String letterText) {
        driver = getDriver();
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        InboxPage inboxPage = loginPage.successLogin(email, password);

        LeftToolBarPage leftToolBarPage = PageFactory.initElements(inboxPage.waiterSendButton(), LeftToolBarPage.class);
        waiterClickOnWriteLetterButton();
        leftToolBarPage.clickOnWriteLetterButton();

        WriteLetterPage writeLetterPage = PageFactory.initElements(driver, WriteLetterPage.class);
        writeLetterPage.waiterWriteLetterPage();
        writeLetterPage.createDraftMessage(email, letterSubject, letterText);

        waiterClickOnDraftsLettersButton();
        DraftsPage draftsPage = leftToolBarPage.clickOnDraftsLettersButton();
        draftsPage.waiterForDeleteMessage(letterSubject);
        draftsPage.deleteMessage(letterSubject);
        waiterClickOnDeletedLettersButton();

        DeletedPage deletedPage = new DeletedPage(driver);
        waiterClickOnDeletedLettersButton();
        leftToolBarPage.clickOnDeletedLettersButton();

        Assert.assertEquals(deletedPage.checkMessage(letterSubject, letterText), true);
        deletedPage.deleteMessage(letterSubject);
        deletedPage.waiterForDeleteMessage(letterSubject);
        deletedPage.waiterCheckDeletedMessage(letterSubject);
        Assert.assertEquals(deletedPage.checkDeletedMessage(letterSubject, letterText), false);
    }
}
