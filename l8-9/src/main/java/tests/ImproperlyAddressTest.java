package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.*;

import java.util.Random;

public class ImproperlyAddressTest extends BaseTest {
    WebDriver driver;
    final Random random = new Random();

    @DataProvider(name = "Data for ImproperlyAddress test")
    public Object[][] dataProperlySendTest() {
        return new Object[][]{
                {"vasil_pupkin2018@mail.ru", "ekscentrisitet2018", String.valueOf(random.nextInt())}
        };
    }

    @Test(dataProvider = "Data for ImproperlyAddress test", groups = "tests")
    public void properlySendTest(String email, String password, String invalidEmail) {
        driver = getDriver();
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        InboxPage inboxPage = loginPage.successLogin(email, password);

        LeftToolBarPage leftToolBarPage = PageFactory.initElements(inboxPage.waiterSendButton(), LeftToolBarPage.class);
        waiterClickOnWriteLetterButton();
        leftToolBarPage.clickOnWriteLetterButton();

        WriteLetterPage writeLetterPage = PageFactory.initElements(driver, WriteLetterPage.class);
        writeLetterPage.waiterWriteLetterPage();
        writeLetterPage.sendMessage(invalidEmail, "", "");

        Assert.assertEquals(writeLetterPage.checkInvalidEmailAlert(), false);
    }
}
