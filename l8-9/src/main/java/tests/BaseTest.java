package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

import static pages.LeftToolBarPage.WAIT_TIME_OUT;

public class BaseTest {

    public static final int IMPLICITLY_WAIT = 10;
    public static final int IMPLICITLY_PAGE_WAIT = 20;
    WebDriver driver;

    @BeforeMethod
    public void setUpDriver() {
        System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(IMPLICITLY_PAGE_WAIT, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(IMPLICITLY_WAIT, TimeUnit.SECONDS);
        driver.get("http://mail.ru");
        driver.manage().window().maximize();
    }

    public WebDriver getDriver() {
        return driver;
    }

    @AfterMethod
    public void tearDown() {
        driver.close();
        driver.quit();
    }

    public void waiterClickOnWriteLetterButton() {
        WebDriverWait waitForOne = new WebDriverWait(driver, WAIT_TIME_OUT);
        try {
            waitForOne.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@data-name='compose']")));
        } catch (TimeoutException e) {
            System.out.println(e);
        }
    }

    public void waiterClickOnOutboxLetterButton() {
        WebDriverWait waitForOne = new WebDriverWait(driver, WAIT_TIME_OUT);
        try {
            waitForOne.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@data-id='500000']")));
        } catch (TimeoutException e) {
            System.out.println(e);
        }
    }

    public void waiterClickOnDeletedLettersButton() {
        WebDriverWait waitForOne = new WebDriverWait(driver, WAIT_TIME_OUT);
        try {
            waitForOne.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@data-id='500002']")));
        } catch (TimeoutException e) {
            System.out.println(e);
        }
    }

    public void waiterClickOnDraftsLettersButton() {
        WebDriverWait waitForOne = new WebDriverWait(driver, WAIT_TIME_OUT);
        try {
            waitForOne.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@data-id='500001']")));
        } catch (TimeoutException e) {
            System.out.println(e);
        }
    }
}
