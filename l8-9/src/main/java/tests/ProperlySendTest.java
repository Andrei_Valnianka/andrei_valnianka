package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.*;

import java.util.Random;

public class ProperlySendTest extends BaseTest {

    WebDriver driver;
    final Random random = new Random();

    @DataProvider(name = "Data for ProperlySend test")
    public Object[][] dataProperlySendTest() {
        return new Object[][]{
                {"vasil_pupkin2018@mail.ru", "ekscentrisitet2018", "Invitation" + String.valueOf(random.nextInt()),
                "Welcome to the hotel California" + String.valueOf(random.nextInt())}
        };
    }

    @Test(dataProvider = "Data for ProperlySend test", groups = "tests")
    public void properlySendTest(String email, String password, String letterSubject, String letterText) {
        driver = getDriver();
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        InboxPage inboxPage = loginPage.successLogin(email, password);

        LeftToolBarPage leftToolBarPage = PageFactory.initElements(inboxPage.waiterSendButton(), LeftToolBarPage.class);
        waiterClickOnWriteLetterButton();
        leftToolBarPage.clickOnWriteLetterButton();

        WriteLetterPage writeLetterPage = PageFactory.initElements(driver, WriteLetterPage.class);
        writeLetterPage.waiterWriteLetterPage();
        driver = writeLetterPage.sendMessage(email, letterSubject, letterText);

        inboxPage = leftToolBarPage.clickOnInboxLettersButton();
        inboxPage.waiterInboxPage();

        Assert.assertEquals(inboxPage.checkMessage(letterSubject, letterText), true);
        OutboxPage outboxPage = leftToolBarPage.clickOnOutboxLettersButton();
        Assert.assertEquals(outboxPage.checkMessage(letterSubject, letterText), true);
    }
}
