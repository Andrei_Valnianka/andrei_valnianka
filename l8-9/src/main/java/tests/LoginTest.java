package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.InboxPage;
import pages.LoginPage;

public class LoginTest extends BaseTest {

    public static final String E_MAIL = "vasil_pupkin2018@mail.ru";
    public static final String CORRECT_PASSWORD = "ekscentrisitet2018";

    WebDriver driver;

    @DataProvider(name = "Data for login test")
    public Object[][] dataForLoginTest() {
        return new Object[][]{
                {"vasil_pupkin2018@mail.ru", "ekscentrisitet2018", "vasil_pupkin2018@mail.ru"},
                {"", "ekscentrisitet2018", ""},
                {"vasil_pupkin2018@mail.ru", "", ""},
                {"vasil_pupkin2018@mail.ru", "ekscentrisitet2018", "vasil_pupkin2018@mail.ru"}
        };
    }

    @Test(dataProvider = "Data for login test", groups = "tests")
    public void loginTest(String email, String password, String expectedResult) {
        driver = getDriver();
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        InboxPage inboxPage = loginPage.successLogin(email, password);
        inboxPage.waiterInboxPage();
        Assert.assertEquals(inboxPage.getUserName(), expectedResult);
    }
}
