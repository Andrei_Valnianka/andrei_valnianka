package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.*;

public class SendMailWithoutSubjectAndBodyTest extends BaseTest {

    @DataProvider(name = "Data for ProperlySend test")
    public Object[][] dataProperlySendTest() {
        return new Object[][]{
                {"vasil_pupkin2018@mail.ru", "ekscentrisitet2018", "", ""}
        };
    }

    @Test(dataProvider = "Data for ProperlySend test", groups = "tests")
    public void properlySendTest(String email, String password, String letterSubject, String letterText) {
        driver = getDriver();

        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        InboxPage inboxPage = loginPage.successLogin(email, password);

        LeftToolBarPage leftToolBarPage = PageFactory.initElements(inboxPage.waiterSendButton(), LeftToolBarPage.class);
        waiterClickOnWriteLetterButton();
        leftToolBarPage.clickOnWriteLetterButton();

        WriteLetterPage writeLetterPage = PageFactory.initElements(driver, WriteLetterPage.class);
        writeLetterPage.waiterWriteLetterPage();
        driver = writeLetterPage.sendMessage(email, letterSubject, letterText);

        driver.findElement(By.xpath(".//*[@id='MailRuConfirm']/div/div[3]/form/div[2]/button[1]")).click();
        inboxPage = leftToolBarPage.clickOnInboxLettersButton();
        inboxPage.waiterInboxPage();

        Assert.assertEquals(inboxPage.checkEmptyMessage(letterSubject, letterText), true);
        waiterClickOnOutboxLetterButton();
        OutboxPage outboxPage = leftToolBarPage.clickOnOutboxLettersButton();
        Assert.assertEquals(outboxPage.checkEmptyMessage(letterSubject, letterText), true);
    }
}
