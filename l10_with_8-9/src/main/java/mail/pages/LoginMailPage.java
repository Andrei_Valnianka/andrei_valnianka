package mail.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginMailPage extends BaseMailPage {

    private static final String INVALID_MESSAGE_LOCATOR = "//div[@id='mailbox:authfail']";
    private static final String LOGIN_LOCATOR = "//*[@id='PH_user-email']";

    @FindBy(xpath = "//*[@id='mailbox__login']")
    private WebElement loginInput;

    @FindBy(xpath = "//*[@id='mailbox__password']")
    private WebElement passwordInput;

    @FindBy(xpath = "//*[@id='mailbox__auth__button']")
    private WebElement submitButton;

    public LoginMailPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public LoginMailPage inputLogin(String email) {
        loginInput.sendKeys(email);
        return this;
    }

    public LoginMailPage inputPassword(String password) {
        passwordInput.sendKeys(password);
        return this;
    }

    public LoginMailPage clickSubmitButton() {
        submitButton.click();
        return this;
    }

    public String getTextOfInvalidMessage() {
        return driver.findElement(By.xpath(INVALID_MESSAGE_LOCATOR)).getText();
    }

    public String getLoginName() {
        waitForAppear(driver, By.xpath(LOGIN_LOCATOR));
        return driver.findElement(By.xpath(LOGIN_LOCATOR)).getText();
    }
}
