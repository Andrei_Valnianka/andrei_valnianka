package mail.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BaseMailPage {

    private static final int WAIT_TIME_OUT = 20;

    protected WebDriver driver;

    public BaseMailPage(WebDriver driver) {
        this.driver = driver;
    }

    public static void waitForAppear(WebDriver driver, By locator) {
        WebDriverWait waitForOne = new WebDriverWait(driver, WAIT_TIME_OUT);
        waitForOne.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }
}
