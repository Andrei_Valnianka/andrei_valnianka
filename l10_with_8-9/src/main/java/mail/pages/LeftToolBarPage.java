package mail.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LeftToolBarPage extends BaseMailPage {

    private static final String WRITE_LETTER_BUTTON_LOCATOR = "//a[@data-name='compose']";
    private static final String INBOX_LETTERS_BUTTON_LOCATOR = "//div[@data-id='0']";
    private static final String OUTBOX_LETTERS_BUTTON_LOCATOR = "//div[@data-id='500000']";
    private static final String DELETE_LETTERS_BUTTON_LOCATOR = "//div[@data-id='500002']";
    private static final String DRAFT_LETTERS_BUTTON_LOCATOR = "//div[@data-id='500001']";

    @FindBy(xpath = WRITE_LETTER_BUTTON_LOCATOR)
    private WebElement writeLetterButton;

    @FindBy(xpath = INBOX_LETTERS_BUTTON_LOCATOR)
    private WebElement inboxLettersButton;

    @FindBy(xpath = OUTBOX_LETTERS_BUTTON_LOCATOR)
    private WebElement outboxLettersButton;

    @FindBy(xpath = DELETE_LETTERS_BUTTON_LOCATOR)
    private WebElement deletedLettersButton;

    @FindBy(xpath = DRAFT_LETTERS_BUTTON_LOCATOR)
    private WebElement draftsLettersButton;

    public LeftToolBarPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public LeftToolBarPage clickOnWriteLetterButton() {
        waitForAppear(driver, By.xpath(WRITE_LETTER_BUTTON_LOCATOR));
        writeLetterButton.click();
        return this;
    }

    public LeftToolBarPage clickOnInboxLettersButton() {
        waitForAppear(driver, By.xpath(INBOX_LETTERS_BUTTON_LOCATOR));
        driver.findElement(By.xpath(INBOX_LETTERS_BUTTON_LOCATOR)).click();
        return this;
    }

    public LeftToolBarPage clickOnOutboxLettersButton() {
        waitForAppear(driver, By.xpath(OUTBOX_LETTERS_BUTTON_LOCATOR));
        outboxLettersButton.click();
        return this;
    }

    public LeftToolBarPage clickOnDeletedLettersButton() {
        waitForAppear(driver, By.xpath(DELETE_LETTERS_BUTTON_LOCATOR));
        deletedLettersButton.click();
        return this;
    }

    public LeftToolBarPage clickOnDraftsLettersButton() {
        waitForAppear(driver, By.xpath(DRAFT_LETTERS_BUTTON_LOCATOR));
        draftsLettersButton.click();
        return this;
    }
}
