package mail.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WriteLetterPage extends BaseMailPage {

    private static final String INPUT_FOR_ADDRESS_LOCATOR = "//textarea[@data-original-name='To']";
    private static final String INPUT_FOR_SUBJECT_LOCATOR = "//input[@name='Subject']";
    private static final String INPUT_FOR_TEXT_LOCATOR = "//body[@id='tinymce']";
    private static final String SEND_BUTTON_LOCATOR = "//*[@data-name='send']";
    private static final String SAVE_DRAFT_BUTTON_LOCATOR =
            " //a[@data-name='saveDraft']";
    private static final String SEND_EMAIL_WITHOUT_SUBJECT_CONFIRMATION_BUTTON_LOCATOR =
            "//div[@class='is-compose-empty_in']//button[@type='submit']";
    private static final String SAVE_DROP_DOWN_MENU_LOCATOR = "//div[@data-group='save-more']";
    private static final int FRAME_INDEX = 0;

    @FindBy(xpath = INPUT_FOR_ADDRESS_LOCATOR)
    private WebElement address;

    @FindBy(xpath = INPUT_FOR_SUBJECT_LOCATOR)
    private WebElement subject;

    @FindBy(xpath = INPUT_FOR_TEXT_LOCATOR)
    private WebElement text;

    @FindBy(xpath = SEND_BUTTON_LOCATOR)
    private WebElement sendButton;

    /*@FindBy(xpath = SAVE_DRAFT_BUTTON_LOCATOR)
    private WebElement saveDraftButton;*/

    public WriteLetterPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public WriteLetterPage addressInput(String email) {
        waitForAppear(driver, By.xpath(INPUT_FOR_ADDRESS_LOCATOR));
        address.sendKeys(email);
        return this;
    }

    public WriteLetterPage subjectInput(String letterSubject) {
        waitForAppear(driver, By.xpath(INPUT_FOR_SUBJECT_LOCATOR));
        subject.sendKeys(letterSubject);
        return this;
    }

    public WriteLetterPage letterTextInput(String letterText) {
        driver.switchTo().frame(FRAME_INDEX);
        waitForAppear(driver, By.xpath(INPUT_FOR_TEXT_LOCATOR));
        text.sendKeys(letterText);
        driver.switchTo().defaultContent();
        return this;
    }

    public WriteLetterPage clickOnSendButton() {
        waitForAppear(driver, By.xpath(SEND_BUTTON_LOCATOR));
        sendButton.click();
        return this;
    }

    public WriteLetterPage clickOnSaveDraftButton() {
        waitForAppear(driver, By.xpath(SAVE_DRAFT_BUTTON_LOCATOR));
        driver.findElement(By.xpath(SAVE_DRAFT_BUTTON_LOCATOR)).click();
        return this;
    }

    public String getTextOfInvalidEmailAlert() {
        return driver.switchTo().alert().getText();
    }

    public void clickOnSendEmailWithoutSubjectAndBody() {
        waitForAppear(driver, By.xpath(SEND_EMAIL_WITHOUT_SUBJECT_CONFIRMATION_BUTTON_LOCATOR));
        driver.findElement(By.xpath(SEND_EMAIL_WITHOUT_SUBJECT_CONFIRMATION_BUTTON_LOCATOR)).click();
    }

    public WriteLetterPage clickOnSaveDropDownMenu() {
        waitForAppear(driver, By.xpath(SAVE_DROP_DOWN_MENU_LOCATOR));
        driver.findElement(By.xpath(SAVE_DROP_DOWN_MENU_LOCATOR)).click();
        return this;
    }
}
