package mail.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;

public class ListOfMailsPage extends BaseMailPage {

    private static final String DELETE_BUTTON_LOCATOR = "//div[@id='b-toolbar__right']//div[contains"
            + "(@data-cache-key,'undefined_false') "
            + "and not (contains(@style, 'display: none;'))]//div[@data-name='remove']";
    private static final String LETTERS_CHECKBOX_LOCATOR = "//a[@data-subject='%s']//div[@data-bem='b-checkbox']";
    private static final String MESSAGE_EMPTY_SUBJECT_LOCATOR = "//*[@class='b-datalist__item__subj' "
            + "and text()='<Без темы>']";
    private static final String MESSAGE_EMPTY_TEXT_LOCATOR = "//span[@class='b-datalist__item__subj__snippet']";
    private static final String MESSAGE_SUBJECT_IN_DELETED_PATTERN =
            "//div[@data-cache-key='500002_undefined_false']//*[@class='b-datalist__item__subj' and text()='%s']";
    private static final String MESSAGE_SUBJECT_IN_INBOX_PATTERN =
            "//div[@data-cache-key='0_undefined_false']//*[@class='b-datalist__item__subj' and text()='%s']";
    private static final String MESSAGE_SUBJECT_IN_OUTBOX_PATTERN =
            "//div[@data-cache-key='500000_undefined_false']//*[@class='b-datalist__item__subj' and text()='%s']";
    private static final String MESSAGE_TEXT_N_INBOX_PATTERN =
            "//div[@data-cache-key='0_undefined_false']//span[@class='b-datalist__item__subj__snippet' "
            + "and text()='%s']";
    private static final String MESSAGE_TEXT_IN_OUTBOX_PATTERN =
            "//div[@data-cache-key='500000_undefined_false']//span[@class='b-datalist__item__subj__snippet' "
            + "and text()='%s']";
    private static final String MESSAGE_TEXT_IN_DELETED_PATTERN = "//div[@data-cache-key='500002_undefined_false']"
            + "//span[@class='b-datalist__item__subj__snippet' and text()='%s']";

    public ListOfMailsPage(WebDriver driver) {
        super(driver);
    }

    public ListOfMailsPage deleteMessage() {
        waitForAppear(driver, By.xpath(String.format(DELETE_BUTTON_LOCATOR)));
        driver.findElement(By.xpath(String.format(DELETE_BUTTON_LOCATOR)))
                .click();
        return this;
    }

    public ListOfMailsPage markMessage(String letterSubject) {

        waitForAppear(driver, By.xpath(String.format(LETTERS_CHECKBOX_LOCATOR, letterSubject)));
        driver.findElement(By.xpath(String.format(LETTERS_CHECKBOX_LOCATOR, letterSubject)))
                .click();
        return this;
    }

    public boolean isEmptyMessageDisplayed() {
        return driver.findElement(By.xpath(MESSAGE_EMPTY_SUBJECT_LOCATOR))
                .isEnabled() && driver.findElement(By.xpath(MESSAGE_EMPTY_TEXT_LOCATOR))
                .isEnabled();
    }

    public boolean isMessageDisplayedInInbox(String letterSubject, String letterText) {
        waitForAppear(driver, By.xpath(String.format(MESSAGE_SUBJECT_IN_INBOX_PATTERN, letterSubject)));
        return driver.findElement(By.xpath(String.format(MESSAGE_SUBJECT_IN_INBOX_PATTERN, letterSubject)))
                .isDisplayed() && (driver.findElement(By.xpath(String.format(MESSAGE_TEXT_N_INBOX_PATTERN, letterText)))
                .isDisplayed());
    }

    public boolean isMessageDisplayedInOutbox(String letterSubject, String letterText) {
        waitForAppear(driver, By.xpath(String.format(MESSAGE_SUBJECT_IN_OUTBOX_PATTERN, letterSubject)));
        return driver.findElement(By.xpath(String.format(MESSAGE_SUBJECT_IN_OUTBOX_PATTERN, letterSubject)))
                .isDisplayed() && (driver.findElement(By.xpath(String.format(
                        MESSAGE_TEXT_IN_OUTBOX_PATTERN, letterText))).isDisplayed());
    }

    public boolean isDeletedMessageDisplayed(String letterSubject, String letterText) {
        try {
            return driver.findElement(By.xpath(String.format(MESSAGE_SUBJECT_IN_DELETED_PATTERN, letterSubject)))
                    .isDisplayed() && (driver.findElement(By.xpath(String.format(
                            MESSAGE_TEXT_IN_DELETED_PATTERN, letterText))).isDisplayed());
        } catch (NoSuchElementException e) {
            return false;
        }
    }
}
