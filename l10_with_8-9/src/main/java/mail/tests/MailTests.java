package mail.tests;

import mail.pages.LeftToolBarPage;
import mail.pages.ListOfMailsPage;
import mail.pages.LoginMailPage;
import mail.pages.WriteLetterPage;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static utils.RandomUtils.getRandomName;

public class MailTests extends mail.tests.BaseTest {

    public void loginMail(String email, String password) {
        LoginMailPage loginMailPage = new LoginMailPage(driver);
        loginMailPage.inputLogin(email)
                .inputPassword(password)
                .clickSubmitButton();
    }

    @DataProvider(name = "Data for ProperlySend test")
    public Object[][] dataForProperlySendTest() {
        return new Object[][]{
                {"vasil_pupkin2018@mail.ru", "ekscentrisitet2018", "Invitation" + getRandomName(),
                "Welcome to the hotel California" + getRandomName()}
        };
    }

    @Test(dataProvider = "Data for ProperlySend test")
    public void properlySendTest(String email, String password, String letterSubject, String letterText) {
        loginMail(email, password);
        LeftToolBarPage leftToolBarPage = new LeftToolBarPage(driver);
        new LeftToolBarPage(driver).clickOnWriteLetterButton();
        WriteLetterPage writeLetterPage = new WriteLetterPage(driver);
        writeLetterPage.addressInput(email)
                .subjectInput(letterSubject)
                .letterTextInput(letterText)
                .clickOnSendButton();
        leftToolBarPage.clickOnInboxLettersButton();
        ListOfMailsPage listOfMailsPage = new ListOfMailsPage(driver);
        StringBuilder log = new StringBuilder();
        log.append("");
        if (!listOfMailsPage.isMessageDisplayedInInbox(letterSubject, letterText)) {
            log.append("Letter is not present in inbox->");
        }
        leftToolBarPage.clickOnOutboxLettersButton();
        if (!listOfMailsPage.isMessageDisplayedInOutbox(letterSubject, letterText)) {
            log.append("Letter is not present in outbox->");
        }
        Assert.assertEquals(log.toString(), "");
    }

    @Test
    public void improperlySendTest() {
        loginMail(CORRECT_EMAIL, CORRECT_PASSWORD);
        LeftToolBarPage leftToolBarPage = new LeftToolBarPage(driver);
        leftToolBarPage.clickOnWriteLetterButton();
        WriteLetterPage writeLetterPage = new WriteLetterPage(driver);
        writeLetterPage.addressInput(getRandomName())
                .clickOnSendButton();
        Assert.assertEquals(writeLetterPage.getTextOfInvalidEmailAlert(),
                "В поле «Кому» указан некорректный адрес получателя.\n"
                        + "Исправьте ошибку и отправьте письмо ещё раз.");
    }

    @Test
    public void sendMailWithoutSubjectAndBodyTest() {
        loginMail(CORRECT_EMAIL, CORRECT_PASSWORD);
        LeftToolBarPage leftToolBarPage = new LeftToolBarPage(driver);
        leftToolBarPage.clickOnWriteLetterButton();
        WriteLetterPage writeLetterPage = new WriteLetterPage(driver);
        writeLetterPage.addressInput(CORRECT_EMAIL)
                .clickOnSendButton()
                .clickOnSendEmailWithoutSubjectAndBody();
        ListOfMailsPage listOfMailsPage = new ListOfMailsPage(driver);
        StringBuilder log = new StringBuilder();
        log.append("");
        leftToolBarPage.clickOnInboxLettersButton();
        if (!listOfMailsPage.isEmptyMessageDisplayed()) {
            log.append("Letter is not present in inbox->");
        }
        leftToolBarPage.clickOnOutboxLettersButton();
        if (!listOfMailsPage.isEmptyMessageDisplayed()) {
            log.append("Letter is not present in outbox->");
        }
        Assert.assertEquals(log.toString(), "");
    }

    @Test(dataProvider = "Data for ProperlySend test")
    public void draftMailTest(String email, String password, String letterSubject, String letterText) {
        loginMail(email, password);
        LeftToolBarPage leftToolBarPage = new LeftToolBarPage(driver);
        leftToolBarPage.clickOnWriteLetterButton();
        WriteLetterPage writeLetterPage = new WriteLetterPage(driver);
        writeLetterPage.addressInput(email)
                .subjectInput(letterSubject)
                .letterTextInput(letterText)
                .clickOnSaveDropDownMenu()
                .clickOnSaveDraftButton();
        ListOfMailsPage listOfMailsPage = new ListOfMailsPage(driver);
        leftToolBarPage.clickOnDraftsLettersButton();
        listOfMailsPage.markMessage(letterSubject)
                .deleteMessage();
        leftToolBarPage.clickOnDeletedLettersButton();
        listOfMailsPage.markMessage(letterSubject)
                .deleteMessage();

        StringBuilder log = new StringBuilder();
        log.append("");
        leftToolBarPage.clickOnDeletedLettersButton();
        if (listOfMailsPage.isDeletedMessageDisplayed(letterSubject, letterText)) {
            log.append("Letter in present in deleted->");
        }
        leftToolBarPage.clickOnDraftsLettersButton();
        if (listOfMailsPage.isDeletedMessageDisplayed(letterSubject, letterText)) {
            log.append("Letter present in drafts->");
        }
        Assert.assertEquals(log.toString(), "");
    }
}
