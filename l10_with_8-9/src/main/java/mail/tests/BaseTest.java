package mail.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class BaseTest {
    protected static final String CORRECT_EMAIL = "vasil_pupkin2018@mail.ru";
    protected static final String CORRECT_PASSWORD = "ekscentrisitet2018";

    private static final int IMPLICITLY_WAIT = 10;
    private static final int IMPLICITLY_PAGE_WAIT = 20;
    private static final int IMPLICITLY_SCRIPT_WAIT = 20;

    protected WebDriver driver;

    @BeforeMethod
    public void setUpDriver() {
        System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(IMPLICITLY_PAGE_WAIT, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(IMPLICITLY_SCRIPT_WAIT, TimeUnit.SECONDS);
        driver.get("http://mail.ru");
        driver.manage().window().maximize();
    }

    @AfterMethod
    public void tearDown() {
        driver.close();
        driver.quit();
    }
}
