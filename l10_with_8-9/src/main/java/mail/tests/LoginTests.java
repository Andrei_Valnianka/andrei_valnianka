package mail.tests;

import mail.pages.LoginMailPage;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {

    public void loginMail(String email, String password) {
        LoginMailPage loginMailPage = new LoginMailPage(driver);
        loginMailPage.inputLogin(email)
                .inputPassword(password)
                .clickSubmitButton();
    }

    @Test(description = "Test for correct login")
    public void correctLoginTest() {
        loginMail(CORRECT_EMAIL, CORRECT_PASSWORD);
        Assert.assertEquals(new LoginMailPage(driver).getLoginName(), CORRECT_EMAIL);
    }

    @DataProvider(name = "Data for negative login test")
    public Object[][] dataForNegativeLoginTest() {
        return new Object[][]{
                {"", "ekscentrisitet2018", "Введите имя ящика"},
                {"", "", "Введите имя ящика"},
                {"vasil_pupkin2018@mail.ru", "", "Введите пароль"}
        };
    }

    @Test(dataProvider = "Data for negative login test")
    public void negativeLoginTest(String email, String password, String expectedResult) {
        loginMail(email, password);
        Assert.assertEquals(new LoginMailPage(driver).getTextOfInvalidMessage(), expectedResult);
    }
}
