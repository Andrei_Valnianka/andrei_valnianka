package cloud.tests;

import cloud.pages.CloudPublicPage;
import cloud.pages.LoginPage;
import cloud.pages.MainCloudPage;
import cloud.pages.TopToolBarPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

import static utils.RandomUtils.createFile;
import static utils.RandomUtils.getRandomName;

public class FileTests extends cloud.tests.BaseTest {

    String folderName;

    public void loginCloud(String login, String password) {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.clickOnEnterToClodButton()
                .setLogin(login)
                .setPassword(password)
                .clickOnSubmitButton();
    }

    @Test
    public void createFolderTest() {
        loginCloud(CORRECT_EMAIL, CORRECT_PASSWORD);
        folderName = getRandomName();
        TopToolBarPage topToolBarPage = new TopToolBarPage(driver);
        topToolBarPage.clickOnCreateButton()
                .clickOnCreateFolderButton()
                .enterNameOfNewFolder(folderName)
                .confirmNameOfNewFolder();
        Assert.assertTrue(new MainCloudPage(driver).isFolderPresentInRootFolder(folderName));
    }

    @Test(dependsOnMethods = "createFolderTest")
    public void deleteFolderTest() {
        loginCloud(CORRECT_EMAIL, CORRECT_PASSWORD);
        MainCloudPage mainCloudPage = new MainCloudPage(driver);
        TopToolBarPage topToolBarPage = new TopToolBarPage(driver);
        mainCloudPage.markTheElement(folderName);
        topToolBarPage.clickOnDeleteButton()
                .clickOnRemoveConfirmationButton();
        mainCloudPage.goToMainCloudPage();
        Assert.assertTrue(!mainCloudPage.isFolderPresentInRootFolder(folderName));
    }

    @Test
    public void uploadFileTest() throws IOException {
        loginCloud(CORRECT_EMAIL, CORRECT_PASSWORD);
        File file = createFile();
        String fileName = file.getName();
        MainCloudPage mainCloudPage = new MainCloudPage(driver);
        new TopToolBarPage(driver).clickOnAddButton();
        mainCloudPage.setPathInDropZone(file);
        Assert.assertTrue(mainCloudPage.isUploadFileDisplayed(fileName));
    }

    @Test
    public void dragAndDropTest() throws IOException, InterruptedException {
        loginCloud(CORRECT_EMAIL, CORRECT_PASSWORD);
        MainCloudPage mainCloudPage = new MainCloudPage(driver);
        TopToolBarPage topToolBarPage = new TopToolBarPage(driver);
        folderName = getRandomName();
        topToolBarPage.clickOnCreateButton()
                .clickOnCreateFolderButton()
                .enterNameOfNewFolder(folderName)
                .confirmNameOfNewFolder();
        File file = createFile();
        String fileName = file.getName();
        topToolBarPage.clickOnAddButton();
        mainCloudPage.setPathInDropZone(file)
                .drugAndDrop(folderName, fileName);
        topToolBarPage.clickMoveConfirmationButton();
        mainCloudPage.clickOnWebElement(folderName);
        Assert.assertTrue(mainCloudPage.isFilePresentInFolder(fileName, folderName));
    }

    @Test
    public void linkCloudTest() {
        loginCloud(CORRECT_EMAIL, CORRECT_PASSWORD);
        MainCloudPage mainCloudPage = new MainCloudPage(driver);
        TopToolBarPage topToolBarPage = new TopToolBarPage(driver);
        folderName = getRandomName();
        topToolBarPage.clickOnCreateButton()
                .clickOnCreateFolderButton()
                .enterNameOfNewFolder(folderName)
                .confirmNameOfNewFolder();
        mainCloudPage.contextClick(folderName)
                .clickOnShareLinkInContextMenu()
                .clickOnShareLinkConfirmationButton();
        String link = mainCloudPage.getUrlText();
        mainCloudPage.openLink(link);
        Assert.assertEquals(new CloudPublicPage().getTitleElementText(driver), folderName);
    }
}
