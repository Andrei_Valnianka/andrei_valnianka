package cloud.tests;

import cloud.pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {

    public void loginCloud(String login, String password) {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.clickOnEnterToClodButton()
                .setLogin(login)
                .setPassword(password)
                .clickOnSubmitButton();
    }

    @Test
    public void loginTest() {
        loginCloud(CORRECT_EMAIL, CORRECT_PASSWORD);
        Assert.assertEquals(new LoginPage(driver).getLoginText(), CORRECT_EMAIL);
    }

    @DataProvider(name = "Data login with empty field test")
    public Object[][] dataForLoginWithEmptyFieldTest() {
        return new Object[][]{
                {"vasil_pupkin2018@mail.ru", "",
                "Введите логин и пароль от своего почтового ящика для того, чтобы продолжить работу с сервисом."},
                {"", "ekscentrisitet2018", "Введите логин и пароль от своего почтового ящика для того, чтобы "
                    + "продолжить работу с сервисом."}
        };
    }

    @Test(dataProvider = "Data login with empty field test")
    public void loginWithEmptyFieldTest(String login, String password, String expectedResult) {
        loginCloud(login, password);
        Assert.assertEquals(new LoginPage(driver).getTextInvalidLoginMessage(), expectedResult);
    }

    @DataProvider(name = "Data for invalid login test")
    public Object[][] dataForInvalidLoginTest() {
        return new Object[][]{
            {"vasil_pupkin2018@mail.ru", "abyrvalg", true}
        };
    }

    @Test(dataProvider = "Data for invalid login test")
    public void invalidLoginTest(String login, String password, boolean expectedResult) {
        loginCloud(login, password);
        Assert.assertEquals(new LoginPage(driver).isInvalidMessagePresent(), expectedResult);
    }
}
