package cloud.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TopToolBarPage extends BasePage {

    private static final By CREATE_BUTTON_LOCATOR = By.xpath(
            "//div[@id='toolbar-left']//div[@class='b-dropdown b-dropdown_create-document-dropdown']");
    private static final By CREATE_FOLDER_LOCATOR = By.xpath(
            "//div[@id='toolbar-left']//*[@data-name='folder']");
    private static final By INPUT_NAME_OF_NEW_FOLDER_LOCATOR = By.xpath("//*[@class='layer__input']");
    private static final By SUBMIT_NAME_OF_NEW_FOLDER_LOCATOR = By.xpath(
            "//div[@class='b-layer__controls__buttons']/*[@data-name='add']");
    private static final By DELETE_BUTTON_LOCATOR = By.xpath(
            "//div[@data-name='remove' and not (contains (@aria-disabled,'disabled'))]");
    private static final By REMOVE_CONFIRMATION_BUTTON_LOCATOR = By.xpath(
            "//div[@class='layer_remove']//button[@data-name='remove']");
    private static final By ADD_BUTTON_LOCATOR = By.xpath(
            "//*[@class='b-sticky' and not (contains(@style, 'hidden'))]//"
                    + "div[@data-name='upload' and not (contains(@class, 'drop-zone'))]");
    private static final By MOVE_CONFIRMATION_BUTTON_LOCATOR = By.xpath(
            "//div[@class='b-layer__container']//button[@data-name='move']");

    public TopToolBarPage(WebDriver driver) {
        super(driver);
    }

    public TopToolBarPage clickOnCreateButton() {
        highlightingClick(driver, CREATE_BUTTON_LOCATOR);
        return this;
    }

    public TopToolBarPage clickOnCreateFolderButton() {
        highlightingClick(driver, CREATE_FOLDER_LOCATOR);
        return this;
    }

    public TopToolBarPage enterNameOfNewFolder(String folderName) {
        driver.findElement(INPUT_NAME_OF_NEW_FOLDER_LOCATOR).sendKeys(folderName);
        return this;
    }

    public TopToolBarPage confirmNameOfNewFolder() {
        highlightingClick(driver, SUBMIT_NAME_OF_NEW_FOLDER_LOCATOR);
        return this;
    }

    public TopToolBarPage clickOnDeleteButton() {
        highlightingClick(driver, DELETE_BUTTON_LOCATOR);
        return this;
    }

    public TopToolBarPage clickOnRemoveConfirmationButton() {
        waitForAppear(driver, REMOVE_CONFIRMATION_BUTTON_LOCATOR);
        highlightingClick(driver, REMOVE_CONFIRMATION_BUTTON_LOCATOR);
        return this;
    }

    public TopToolBarPage clickOnAddButton() {
        waitForAppear(driver, ADD_BUTTON_LOCATOR);
        highlightingClick(driver, ADD_BUTTON_LOCATOR);
        return this;
    }

    public TopToolBarPage clickMoveConfirmationButton() {
        waitForAppear(driver, MOVE_CONFIRMATION_BUTTON_LOCATOR);
        highlightingClick(driver, MOVE_CONFIRMATION_BUTTON_LOCATOR);
        return this;
    }
}
