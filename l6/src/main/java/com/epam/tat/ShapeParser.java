package com.epam.tat;

import java.util.ArrayList;

import static jdk.nashorn.internal.objects.NativeString.toUpperCase;

/**
 * Created by VZ-RED on 08.07.2017.
 */
public class ShapeParser {
    ArrayList<Object> shapeCollection = new ArrayList<>();

    public ArrayList<Object> shapeStringParser(ArrayList<String> shapes) {

        for (String str : shapes) {
            ShapesType shapesType = ShapesType.valueOf(toUpperCase(str.replaceAll("[^A-Za-z]", "")));

            switch (shapesType) {
                case TRIANGLE:
                    String triangleString = str.replaceAll("\\D", "");
                    addTriangle(triangleString);
                    break;
                case RECTANGLE:
                    String[] substringsForRectangle = str.split(":");
                    int length = Integer.parseInt(substringsForRectangle[1]);
                    int width = Integer.parseInt(substringsForRectangle[2]);
                    addRectangle(length, width);
                    break;
                case SQUARE:
                    String squareString = str.replaceAll("\\D", "");
                    addSquare(squareString);
                    break;
                case CIRCLE:
                    String circleString = str.replaceAll("\\D", "");
                    addCircle(circleString);
                    break;
                default:
                    System.out.println("Unknown shapes type");
            }
        }
        return shapeCollection;
    }

    public void addTriangle(String triangleString) {
        int value = Integer.parseInt(triangleString);
        Triangle triangle = new Triangle(value);
        shapeCollection.add(triangle);
    }

    public void addRectangle(int length, int width) {
        Rectangle rectangle = new Rectangle(length, width);
        shapeCollection.add(rectangle);
    }

    public void addSquare(String squareString) {
        int value = Integer.parseInt(squareString);
        Square square = new Square(value);
        shapeCollection.add(square);
    }

    public void addCircle(String circleString) {
        int value = Integer.parseInt(circleString);
        Circle circle = new Circle(value);
        shapeCollection.add(circle);
    }
}



