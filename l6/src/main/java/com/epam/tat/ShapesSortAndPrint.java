package com.epam.tat;

import java.util.ArrayList;

/**
 * Created by VZ-RED on 09.07.2017.
 */
public class ShapesSortAndPrint {

    public void sortAndPrint(ArrayList<Object> shapeCollectionIn) {

        ArrayList<Object> shapeCollection = shapeCollectionIn;

        while (shapeCollection.size() > 0) {
            int shapeCollectionLength;
            shapeCollectionLength = shapeCollection.size();
            Object object;

            int maxIndex = 0;
            int maxArea = 0;
            for (int i = 0; i < shapeCollectionLength; i++) {

                object = shapeCollection.get(i);

                if (object.hashCode() > maxArea) {
                    maxIndex = i;
                }
            }
            object = shapeCollection.get(maxIndex);
            System.out.println(object.toString());
            shapeCollection.remove(maxIndex);
        }
    }
}
