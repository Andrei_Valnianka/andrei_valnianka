package com.epam.tat;

import java.util.ArrayList;

/**
 * Created by VZ-RED on 10.07.2017.
 */
public class DataAnalyzer {

    public void dataAnalyzer(ArrayList<String> shapes) {

        ShapeParser parser = new ShapeParser();
        ArrayList<Object> shapeCollection;

        if (shapes.get(0).equals("-plain")) {
            shapes.remove(0);
            shapeCollection = parser.shapeStringParser(shapes);
            ShapesSortAndPrint shapesSortAndPrint = new ShapesSortAndPrint();
            shapesSortAndPrint.sortAndPrint(shapeCollection);
        } else {
            String path = shapes.get(1);
            ShapesFileReader shapesFileReader = new ShapesFileReader();
            ArrayList<String> shapesFromFile = shapesFileReader.stringReader(path);
            shapeCollection = parser.shapeStringParser(shapesFromFile);
            ShapesSortAndPrint shapesSortAndPrint = new ShapesSortAndPrint();
            shapesSortAndPrint.sortAndPrint(shapeCollection);
        }

    }
}
