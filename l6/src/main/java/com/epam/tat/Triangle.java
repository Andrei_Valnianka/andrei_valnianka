package com.epam.tat;

import static java.lang.StrictMath.sqrt;

/**
 * Created by VZ-RED on 06.07.2017.
 */
public class Triangle implements Shape {

    int side;
    final int numberOfSide = 3;
    final int four = 4;

    Triangle(int side) {
        this.side = side;
    }

    @Override
    public int area() {
        return (int) ((sqrt(numberOfSide) * Math.pow(side, 2)) / four);
    }

    @Override
    public int perimeter() {
        return numberOfSide * side;
    }

    @Override
    public String toString() {
        return "Triangle - " + side + "\n         Area: "
                + (int) ((sqrt(numberOfSide) * Math.pow(side, 2)) / four)
                + "\n         Perimeter: " + numberOfSide * side;
    }

    @Override
    public int hashCode() {
        return (int) ((sqrt(numberOfSide) * Math.pow(side, 2)) / four);
    }
}
