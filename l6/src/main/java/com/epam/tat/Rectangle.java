package com.epam.tat;

/**
 * Created by VZ-RED on 06.07.2017.
 */
public class Rectangle implements Shape {

    int length;
    int width;

    Rectangle(int length, int width) {
        this.length = length;
        this.width = width;
    }

    @Override
    public int area() {
        return length * width;
    }

    @Override
    public int perimeter() {
        return 2 * length + 2 * width;
    }

    @Override
    public String toString() {
        return "Rectangle - " + length + ":" + width + "\n         Area: "
                + length * width + "\n         Perimeter: " + (2 * length + 2 * width);
    }

    @Override
    public int hashCode() {
        return length * width;
    }
}
