package com.epam.tat;

/**
 * Created by VZ-RED on 06.07.2017.
 */
public class Square implements Shape {
    int side;
    final int numberOfSide = 4;

    Square(int side) {
        this.side = side;
    }

    @Override
    public int area() {
        return side * side;
    }

    @Override
    public int perimeter() {
        return numberOfSide * side;
    }

    @Override
    public String toString() {
        return "Square - " + side + "\n         Area: " + side * side + "\n         Perimeter: " + numberOfSide * side;
    }

    @Override
    public int hashCode() {
        return side * side;
    }
}
