package com.epam.tat;

import java.util.ArrayList;
import java.util.Scanner;

import static java.lang.System.in;
import static jdk.nashorn.internal.objects.NativeString.toUpperCase;

public class Main {

    public static void main(String[] args) {
        ArrayList<String> shapes = new ArrayList<>();
        for (int i = 0; i < args.length; i++) {
            shapes.add(args[i]);
        }

        DataAnalyzer analyzer = new DataAnalyzer();
        analyzer.dataAnalyzer(shapes);

        stringNameParser();
    }

    public static void stringNameParser() {

        System.out.println("Enter string for parse");
        Scanner scanner = new Scanner(in);
        String stringForParse = scanner.nextLine();

        ShapesType shapeType = ShapesType.valueOf(toUpperCase(stringForParse.replaceAll("[^A-Za-z]", "")));
        switch (shapeType) {
            case TRIANGLE:
                System.out.println("TRIANGLE");
                break;
            case RECTANGLE:
                System.out.println("RECTANGLE");
                break;
            case SQUARE:
                System.out.println("SQUARE");
                break;
            case CIRCLE:
                System.out.println("CIRCLE");
                break;
            default:
                System.out.println("Unknown type of shape");
        }
    }

}
