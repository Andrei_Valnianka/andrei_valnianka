package com.epam.tat;

/**
 * Created by VZ-RED on 06.07.2017.
 */
public interface Shape {
    int area();

    int perimeter();
}
