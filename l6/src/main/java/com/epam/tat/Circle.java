package com.epam.tat;

/**
 * Created by VZ-RED on 06.07.2017.
 */
public class Circle implements Shape {
    int radius;
    final double pi = 3.14;

    Circle(int radius) {
        this.radius = radius;
    }

    @Override
    public int area() {
        return (int) (pi * Math.pow(radius, 2));
    }

    @Override
    public int perimeter() {
        return (int) (2 * pi * radius);
    }

    @Override
    public String toString() {
        return "Circle - " + radius + "\n         Area: "
                + (int) (pi * Math.pow(radius, 2)) + "\n         Perimeter: " + (int) (2 * pi * radius);
    }

    @Override
    public int hashCode() {
        return (int) (pi * Math.pow(radius, 2));
    }
}
