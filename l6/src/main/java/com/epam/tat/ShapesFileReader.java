package com.epam.tat;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by VZ-RED on 08.07.2017.
 */
public class ShapesFileReader {

    public ArrayList<String> stringReader(String path) {

        ArrayList<String> shapes = new ArrayList<>();

        FileInputStream fis = null;
        BufferedReader reader = null;

        try {

            fis = new FileInputStream(path);
            reader = new BufferedReader(new InputStreamReader(fis));

            String line = reader.readLine();
            shapes.add(line);
            while (line != null) {
                line = reader.readLine();
                if (line != null) {
                    shapes.add(line);
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();

        }
        try {
            reader.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return shapes;
    }
}

