package com.epam.tat.pages.mail;

import com.epam.tat.framework.ui.Element;
import org.openqa.selenium.By;

public class LeftToolBarPage {

    private final Element writeLettersButton = new Element(By.xpath("//a[@data-name='compose']"));
    private final Element inboxLettersButton = new Element(By.xpath("//div[@data-id='0']"));
    private final Element outboxLettersButton = new Element(By.xpath("//div[@data-id='500000']"));
    private final Element deleteLettersButton = new Element(By.xpath("//div[@data-id='500002']"));
    private final Element draftLettersButton = new Element(By.xpath("//div[@data-id='500001']"));

    public LeftToolBarPage clickOnWriteLetterButton() {
        writeLettersButton.click();
        return this;
    }

    public LeftToolBarPage clickOnInboxLettersButton() {
        inboxLettersButton.click();
        return this;
    }

    public LeftToolBarPage clickOnOutboxLettersButton() {
        outboxLettersButton.click();
        return this;
    }

    public LeftToolBarPage clickOnDeletedLettersButton() {
        deleteLettersButton.click();
        return this;
    }

    public LeftToolBarPage clickOnDraftsLettersButton() {
        draftLettersButton.click();
        return this;
    }
}
