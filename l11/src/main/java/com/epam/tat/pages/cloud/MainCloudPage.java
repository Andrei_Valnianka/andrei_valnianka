package com.epam.tat.pages.cloud;

import com.epam.tat.framework.ui.Browser;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class MainCloudPage {

    private static final String URL_OF_MAIN_CLOUD_PAGE = "https://cloud.mail.ru/home/";
    private static final By ROOT_FOLDER_LOCATOR = By.xpath(
            "//span[@class='breadcrumbs__item__plain-header' and text()='Облако']");
    private static final By DROP_ZONE_LOCATOR = By.xpath(
            "//div[@class='drop-zone']/input[@type='file']");
    private static final By SHARE_LINK_BUTTON_IN_CONTEXT_MENU_LOCATOR = By.xpath(
            "//*[@data-name='publish']");
    private static final By SHARE_LINK_CONFIRMATION_BUTTON_LOCATOR = By.xpath(
            "//button[@data-name='copy']");
    private static final By URL_INPUT_LOCATOR = By.xpath(
            "//div[@class='publishing__url']/input[@data-name='url']");
    private static final String FOLDER_SEARCH_PATTERN = "//div[@class='b-filename__text']/*["
            + "contains (text(), '%s')]";
    private static final String FILE_SEARCH_PATTERN = "//div[@data-name='link' and @data-id='/%s']";
    private static final String SEARCH_FILE_IN_FOLDER_PATTERN = "//div[@data-id='/%s/%s']";
    private static final String MARK_FILE_PATTERN = "//div[@data-id='/%s']//div[@data-bem='b-checkbox']";
    private static final String SEARCH_WEB_ELEMENT_PATTERN = "//div[@class='b-filename__text']/*["
            + "contains (text(), '%s')]";
    private static final String CONTEXT_MENU_PATTERN = "//div[@class='b-filename__text']/*["
            + "contains (text(), '%s')]";
    private static final String ATTRIBUTE_NAME = "value";

    public boolean isUploadFileDisplayed(String fileName) {
        try {
            return Browser.getInstance().waitForAppear(FILE_SEARCH_PATTERN, fileName)
             .isDisplayed(FILE_SEARCH_PATTERN, fileName);
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean isFolderPresentInRootFolder(String folderName) {
        try {
            return Browser.getInstance().waitForAppear(ROOT_FOLDER_LOCATOR)
                    .isDisplayed(FOLDER_SEARCH_PATTERN, folderName);
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean isFilePresentInFolder(String fileName, String folderName) {
        try {
            return Browser.getInstance().isDisplayed(SEARCH_FILE_IN_FOLDER_PATTERN, folderName, fileName);
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public MainCloudPage markTheElement(String folderName) {
        Browser.getInstance()
               .waitForAppear(MARK_FILE_PATTERN, folderName)
                .click(MARK_FILE_PATTERN, folderName);
        return this;
    }

    public MainCloudPage setPathInDropZone(String absolutePath) {
        Browser.getInstance()
                .sendKeys(DROP_ZONE_LOCATOR, absolutePath);
        return this;
    }

    public MainCloudPage clickOnWebElement(String elementName) {
        By locator = By.xpath(String.format(SEARCH_WEB_ELEMENT_PATTERN, elementName));
        Browser.getInstance().click(locator);
        return this;
    }

    public MainCloudPage contextClick(String elementName) {
        WebElement element = Browser.getInstance().getWrappedDriver()
                .findElement(By.xpath(String.format(CONTEXT_MENU_PATTERN, elementName)));
        Actions builder = new Actions(Browser.getInstance().getWrappedDriver());
        builder.moveToElement(element).contextClick().build().perform();
        return this;
    }

    public MainCloudPage clickOnShareLinkInContextMenu() {
        Browser.getInstance().waitForAppear(SHARE_LINK_BUTTON_IN_CONTEXT_MENU_LOCATOR)
                .click(SHARE_LINK_BUTTON_IN_CONTEXT_MENU_LOCATOR);
        return this;
    }

    public MainCloudPage clickOnShareLinkConfirmationButton() {
        Browser.getInstance().waitForAppear(SHARE_LINK_CONFIRMATION_BUTTON_LOCATOR)
                .click(SHARE_LINK_CONFIRMATION_BUTTON_LOCATOR);
        return this;
    }

    public String getUrlText() {
        return Browser.getInstance().waitForAppear(URL_INPUT_LOCATOR).getAttribute(URL_INPUT_LOCATOR, ATTRIBUTE_NAME);
    }

    public MainCloudPage goToMainCloudPage() {
        Browser.getInstance().navigate(URL_OF_MAIN_CLOUD_PAGE);
        return this;
    }

    public void drugAndDrop(String fileName, String folderName) {
        Browser.getInstance().waitForAppear(FILE_SEARCH_PATTERN, folderName)
                .waitForAppear(FILE_SEARCH_PATTERN, fileName);
        WebElement target = Browser.getInstance().getWebElement(FILE_SEARCH_PATTERN, folderName);
        WebElement source = Browser.getInstance().getWebElement(FILE_SEARCH_PATTERN, fileName);
        Actions builder = new Actions(Browser.getInstance().getWrappedDriver());
        builder.dragAndDrop(source, target).perform();
    }

    public void openLink(String link) {
        Browser.getInstance().navigate(link);
    }
}

