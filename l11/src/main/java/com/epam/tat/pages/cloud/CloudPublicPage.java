package com.epam.tat.pages.cloud;

import com.epam.tat.framework.ui.Element;
import org.openqa.selenium.By;

public class CloudPublicPage {

    private final Element nameOfElement = new Element(By.xpath(
            "//span[@class='breadcrumbs__item__plain-header']"));

    public String getTitleElementText() {
        return nameOfElement.getText();
    }
}
