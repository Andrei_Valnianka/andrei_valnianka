package com.epam.tat.services;

import com.epam.tat.framework.bo.CloudFile;
import com.epam.tat.pages.cloud.CloudPublicPage;
import com.epam.tat.pages.cloud.LoginPage;
import com.epam.tat.pages.cloud.MainCloudPage;
import com.epam.tat.pages.cloud.TopToolBarPage;

import static com.epam.tat.framework.utils.RandomUtils.getRandomName;

public class CloudService {

    String folderName;
    LoginPage loginPage = new LoginPage();
    MainCloudPage mainCloudPage = new MainCloudPage();
    TopToolBarPage topToolBarPage = new TopToolBarPage();

    public String getCloudLoginText() {
        return loginPage.getLoginText();
    }

    public String getTextOfInvalidLoginMessage() {
        return new LoginPage().getTextInvalidLoginMessage();
    }

    public boolean isInvalidAlertDisplayed() {
        return loginPage.isInvalidMessagePresent();
    }

    public String createFolderAndGetName() {
        folderName = getRandomName();
        topToolBarPage.clickOnCreateButton()
                .clickOnCreateFolderButton()
                .enterNameOfNewFolder(folderName)
                .confirmNameOfNewFolder();
        return folderName;
    }

    public boolean isFolderDisplayed() {
        return mainCloudPage.isFolderPresentInRootFolder(folderName);
    }

    public boolean isFolderDisplayedInRoot() {
        return mainCloudPage.isFolderPresentInRootFolder(folderName);
    }

    public void deleteFolder() {
        mainCloudPage.markTheElement(folderName);
        topToolBarPage.clickOnDeleteButton()
                .clickOnRemoveConfirmationButton();
        mainCloudPage.goToMainCloudPage();
    }

    public void uploadFile(CloudFile cloudFile) {
        new TopToolBarPage().clickOnAddButton();
        mainCloudPage.setPathInDropZone(cloudFile.getFilePath());
    }

    public boolean isUploadFileDisplayed(CloudFile cloudFile) {
        return mainCloudPage.isUploadFileDisplayed(cloudFile.getFileName());
    }

    public void drugAndDropFile(CloudFile cloudFile) {
        mainCloudPage.drugAndDrop(cloudFile.getFileName(), folderName);
    }

    public void clickOnMoveConfirmationAndOpenFolder() {
        topToolBarPage.clickMoveConfirmationButton();
        mainCloudPage.clickOnWebElement(folderName);
    }

    public boolean isFileDisplayedInFolder(CloudFile cloudFile) {
        return mainCloudPage.isFilePresentInFolder(cloudFile.getFileName(), folderName);
    }

    public void getLinkAndOpenLink() {
        mainCloudPage.contextClick(folderName)
                .clickOnShareLinkInContextMenu()
                .clickOnShareLinkConfirmationButton();
        String link = mainCloudPage.getUrlText();
        mainCloudPage.openLink(link);
    }

    public String getTitleText() {
        return new CloudPublicPage().getTitleElementText();
    }
}
