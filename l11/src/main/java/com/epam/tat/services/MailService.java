package com.epam.tat.services;

import com.epam.tat.framework.bo.Letter;
import com.epam.tat.pages.mail.LeftToolBarPage;
import com.epam.tat.pages.mail.ListOfMailsPage;
import com.epam.tat.pages.mail.WriteLetterPage;

public class MailService {

    LeftToolBarPage leftToolBarPage = new LeftToolBarPage();
    WriteLetterPage writeLetterPage = new WriteLetterPage();
    ListOfMailsPage listOfMailsPage = new ListOfMailsPage();

    private void fillLetter(Letter letter) {

        leftToolBarPage.clickOnWriteLetterButton();
        writeLetterPage.addressInput(letter.getEmail())
                .subjectInput(letter.getSubject())
                .letterTextInput(letter.getText());
    }

    public void sendLetter(Letter letter) {
        fillLetter(letter);
        writeLetterPage.clickOnSendButton();
        listOfMailsPage.isLetterSent();
    }

    public void sendInvalidLetter(Letter letter) {
        fillLetter(letter);
        writeLetterPage.clickOnSendButton();
    }

    public void sendLetterWithoutSubjectAndBody(Letter letter) {
        fillLetter(letter);
        writeLetterPage.clickOnSendButton();
        writeLetterPage.clickOnSendEmailWithoutSubjectAndBody();
        if (!listOfMailsPage.isLetterSent()) {
            System.out.println("Letter isn't sent");
        }
    }

    public void saveLetterInDraft(Letter letter) {
        fillLetter(letter);
        writeLetterPage.clickOnSaveDropDownMenu()
                .clickOnSaveDraftButton();
        if (!writeLetterPage.isMessageSaveInDraftDisplayed()) {
            System.out.println("Letter isn't save in draft");
        }

    }

    public String getTextOfInvalidMessage() {
        return writeLetterPage.getTextOfInvalidEmailAlert();
    }

    public void deleteLetterInDraft(Letter letter) {
        leftToolBarPage.clickOnDraftsLettersButton();
        listOfMailsPage.markMessage(letter.getSubject())
                .deleteMessage();
    }

    public void deleteLetterInDeleted(Letter letter) {
        leftToolBarPage.clickOnDeletedLettersButton();
        listOfMailsPage.markMessage(letter.getSubject())
                .deleteMessage();
    }

    public String isLetterWithSubjectAndBodyDisplayedInInboxAndOutbox(Letter letter) {

        leftToolBarPage.clickOnInboxLettersButton();
        StringBuilder log = new StringBuilder();
        log.append("");
        if (!listOfMailsPage.isMessageDisplayedInInbox(letter.getSubject(), letter.getText())) {
            log.append("Letter is not present in inbox->");
        }
        leftToolBarPage.clickOnOutboxLettersButton();
        if (!listOfMailsPage.isMessageDisplayedInOutbox(letter.getSubject(), letter.getText())) {
            log.append("Letter is not present in outbox->");
        }
        return log.toString();
    }

    public String isEmptyLetterDisplayedInInboxAndOutbox() {
        StringBuilder log = new StringBuilder();
        log.append("");
        leftToolBarPage.clickOnOutboxLettersButton();
        if (!listOfMailsPage.isEmptyMessageDisplayed()) {
            log.append("Letter is not present in outbox->");
        }
        leftToolBarPage.clickOnInboxLettersButton();
        if (!listOfMailsPage.isEmptyMessageDisplayedInInbox()) {
            log.append("Letter is not present in inbox->");
        }
        return log.toString();
    }

    public String isDeletedLetterDisplayed(Letter letter) {
        StringBuilder log = new StringBuilder();
        log.append("");
        leftToolBarPage.clickOnDeletedLettersButton();
        if (listOfMailsPage.isDeletedMessageDisplayed(letter.getSubject(), letter.getText())) {
            log.append("Letter in present in deleted->");
        }
        leftToolBarPage.clickOnDraftsLettersButton();
        if (listOfMailsPage.isDeletedMessageDisplayed(letter.getSubject(), letter.getText())) {
            log.append("Letter present in drafts->");
        }
        return log.toString();
    }
}
