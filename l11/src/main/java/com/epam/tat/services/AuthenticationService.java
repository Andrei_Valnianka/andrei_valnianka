package com.epam.tat.services;

import com.epam.tat.pages.cloud.LoginPage;
import com.epam.tat.pages.mail.LoginMailPage;

public class AuthenticationService {

    public void loginMail(String email, String password) {
        LoginMailPage loginMailPage = new LoginMailPage();
        loginMailPage.inputLogin(email)
                .inputPassword(password)
                .clickSubmitButton();
    }

    public void loginCloud(String login, String password) {

        LoginPage loginPage = new LoginPage();
        loginPage.clickOnEnterToClodButton()
                .setLogin(login)
                .setPassword(password)
                .clickOnSubmitButton();
    }

    public String getMailLoginName() {
        return new LoginMailPage().getLoginName();
    }

    public String getTextOfInvalidMailMessage() {
        return new LoginMailPage().getTextOfInvalidMessage();
    }
}
