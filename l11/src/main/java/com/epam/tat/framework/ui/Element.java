package com.epam.tat.framework.ui;

import org.openqa.selenium.By;

public class Element {

    private By locator;

    public Element(By locator) {
        this.locator = locator;
    }

    public Element click() {
        Browser.getInstance().waitForAppear(locator).click(locator);
        return this;
    }

    public String getText() {
        return Browser.getInstance().waitForAppear(locator).getText(locator);
    }

    public Element sendKeys(String keys) {
        Browser.getInstance().waitForAppear(locator).sendKeys(locator, keys);
        return this;
    }

    public boolean isDisplayed() {
        return Browser.getInstance().waitForAppear(locator).isDisplayed(locator);
    }

    public void waitForAppear() {
        Browser.getInstance().waitForAppear(locator);
    }
}
