package com.epam.tat.framework.bo;

import static com.epam.tat.framework.utils.RandomUtils.getRandomName;

public class LetterFactory {

    private static final String CORRECT_EMAIL = "vasil_pupkin2018@mail.ru";
    private static final String EMPTY_STRING = "";

    public Letter getLetterWithEmailSubjectBody() {
        return new Letter(CORRECT_EMAIL, "Invitation" + getRandomName(),
                "Welcome to the hotel California" + getRandomName());
    }

    public Letter getLetterWithInvalidAddress() {
        Letter letter = new Letter(CORRECT_EMAIL, EMPTY_STRING, EMPTY_STRING);
        letter.setEmail(getRandomName());
        return letter;
    }

    public Letter getLetterWithoutSubjectAndBody() {
        Letter letter = new Letter(CORRECT_EMAIL, EMPTY_STRING, EMPTY_STRING);
        return letter;
    }
}
