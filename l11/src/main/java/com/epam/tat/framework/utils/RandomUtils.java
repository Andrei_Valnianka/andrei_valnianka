package com.epam.tat.framework.utils;

import java.io.File;
import java.io.IOException;
import java.util.Random;

public class RandomUtils {

    private static final int BOUND = 5;

    public static File createFile() {
        final Random random = new Random();
        File file = new File("to_upload" + String.valueOf(random.nextInt()) + ".txt");
        try {
            file.createNewFile();
            org.apache.commons.io.FileUtils.writeStringToFile(file, "file text" + new Random().nextInt(BOUND));
        } catch (IOException e) {
            throw new RuntimeException("Error writing file " + file.getName());
        }
        return file;
    }

    public static String getRandomName() {
        final Random random = new Random();
        return String.valueOf(random.nextInt());
    }
}
