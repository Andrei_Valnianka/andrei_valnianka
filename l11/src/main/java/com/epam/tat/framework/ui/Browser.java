package com.epam.tat.framework.ui;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public final class Browser implements WrapsDriver {

    private static final int IMPLICITLY_WAIT = 4;
    private static final int IMPLICITLY_PAGE_WAIT = 20;
    private static final int IMPLICITLY_SCRIPT_WAIT = 20;

    private static final int WAIT_TIME_OUT = 10;

    private static Browser instance;
    private WebDriver wrappedWebDriver;

    private Browser() {
        System.setProperty("webdriver.chrome.driver", "./src/main/resources/drivers/chromedriver.exe");
        wrappedWebDriver = new ChromeDriver();
        wrappedWebDriver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT, TimeUnit.SECONDS);
        wrappedWebDriver.manage().timeouts().pageLoadTimeout(IMPLICITLY_PAGE_WAIT, TimeUnit.SECONDS);
        wrappedWebDriver.manage().timeouts().setScriptTimeout(IMPLICITLY_SCRIPT_WAIT, TimeUnit.SECONDS);
    }

    public static Browser getInstance() {
        if (instance == null) {
            instance = new Browser();
        }
        return instance;
    }

    public void stopBrowser() {
        wrappedWebDriver.close();
        wrappedWebDriver.quit();
        instance = null;
    }

    @Override
    public WebDriver getWrappedDriver() {
        return wrappedWebDriver;
    }

    public Browser getUrlAndMaximize(String url) {
        wrappedWebDriver.get(url);
        wrappedWebDriver.manage().window().maximize();
        return this;
    }

    public void highlightingElement(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) wrappedWebDriver;
        js.executeScript("arguments[0].style.border='5px solid red'", element);
    }

    public Browser click(By by) {
        WebElement element = wrappedWebDriver.findElement(by);
        highlightingElement(element);
        wrappedWebDriver.findElement(by).click();
        return this;
    }

    public Browser click(String pattern, String name) {
        WebElement element = wrappedWebDriver.findElement(By.xpath(String.format(pattern, name)));
        highlightingElement(element);
        wrappedWebDriver.findElement(By.xpath(String.format(pattern, name))).click();
        return this;
    }

    public Browser sendKeys(By by, String keys) {
        wrappedWebDriver.findElement(by)
                .sendKeys(keys);
        return this;
    }

    public Browser waitForAppear(By by) {
        WebDriverWait waitForOne = new WebDriverWait(wrappedWebDriver, WAIT_TIME_OUT);
        waitForOne.until(ExpectedConditions.visibilityOfElementLocated(by));
        return this;
    }

    public Browser waitForAppear(String pattern, String name) {
        new WebDriverWait(wrappedWebDriver, WAIT_TIME_OUT)
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(pattern, name))));
        return this;
    }

    public String getText(By by) {
        return wrappedWebDriver.findElement(by).getText();
    }

    public boolean isDisplayed(By by) {
        return wrappedWebDriver.findElement(by).isDisplayed();
    }

    public boolean isDisplayed(String pattern, String name) {
        return wrappedWebDriver.findElement(By.xpath(String.format(pattern, name))).isDisplayed();
    }

    public boolean isDisplayed(String pattern, String folderName, String fileName) {
        return wrappedWebDriver.findElement(By.xpath(String.format(pattern, folderName, fileName))).isDisplayed();
    }

    public String getAttribute(By by, String attributeName) {
        return wrappedWebDriver.findElement(by).getAttribute(attributeName);
    }

    public Browser navigate(String  url) {
        wrappedWebDriver.navigate().to(url);
        return this;
    }

    public WebElement getWebElement(String pattern, String name) {
        return  wrappedWebDriver.findElement(By.xpath(String.format(pattern, name)));
    }
}
