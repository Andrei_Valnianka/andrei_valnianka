package com.epam.tat.tests.cloud;

import com.epam.tat.framework.bo.Account;
import com.epam.tat.framework.bo.AccountFactory;
import com.epam.tat.services.CloudService;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {

    CloudService cloudService = new CloudService();

    @Test(description = "Valid login")
    public void loginTest() {
        authenticationService.loginCloud(correctAccount.getLogin(), correctAccount.getPassword());
        Assert.assertEquals(cloudService.getCloudLoginText(), correctAccount.getLogin());
    }

    @DataProvider(name = "Data login with empty field test")
    public Object[][] dataForLoginWithEmptyFieldTest() {
        return new Object[][]{
                {new AccountFactory().getAccountWithoutPassword(),
                "Введите логин и пароль от своего почтового ящика для того, чтобы продолжить работу с сервисом."},
                {new AccountFactory().getAccountWithoutLogin(),
                "Введите логин и пароль от своего почтового ящика для того, чтобы "
                        + "продолжить работу с сервисом."}
        };
    }

    @Test(description = "Login with empty login or password", dataProvider = "Data login with empty field test")
    public void loginWithEmptyFieldTest(Account account, String expectedResult) {
        authenticationService.loginCloud(account.getLogin(), account.getPassword());
        Assert.assertEquals(cloudService.getTextOfInvalidLoginMessage(), expectedResult);
    }

    @DataProvider(name = "Data for invalid login test")
    public Object[][] dataForInvalidLoginTest() {
        return new Object[][]{
                {new AccountFactory().getAccountWithoutIncorrectPassword(), true}
        };
    }

    @Test(description = "Login with invalid password", dataProvider = "Data for invalid login test")
    public void invalidLoginTest(Account account, boolean expectedResult) {
        authenticationService.loginCloud(account.getLogin(), account.getPassword());
        Assert.assertEquals(cloudService.isInvalidAlertDisplayed(), expectedResult);
    }
}
