package com.epam.tat.tests.cloud;

import com.epam.tat.framework.bo.CloudFile;
import com.epam.tat.framework.bo.CloudFileFactory;
import com.epam.tat.services.CloudService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FileTests extends BaseTest {

    String folderName;
    CloudService cloudService = new CloudService();

    @Test(description = "Test for creating a folder")
    public void createFolderTest() {
        authenticationService.loginCloud(correctAccount.getLogin(), correctAccount.getPassword());
        folderName = cloudService.createFolderAndGetName();
        Assert.assertTrue(cloudService.isFolderDisplayed());
    }

    @Test(description = "Test for deleting a folder")
    public void deleteFolderTest() {
        authenticationService.loginCloud(correctAccount.getLogin(), correctAccount.getPassword());
        folderName = cloudService.createFolderAndGetName();
        cloudService.deleteFolder();
        Assert.assertTrue(!cloudService.isFolderDisplayedInRoot());
    }

    @Test(description = "Test for uploading file")
    public void uploadFileTest() {
        authenticationService.loginCloud(correctAccount.getLogin(), correctAccount.getPassword());
        CloudFile cloudFile = new CloudFileFactory().getValidFile();
        cloudService.uploadFile(cloudFile);
        Assert.assertTrue(cloudService.isUploadFileDisplayed(cloudFile));
    }

    @Test(description = "Drug file and drop in folder")
    public void dragAndDropTest() {
        authenticationService.loginCloud(correctAccount.getLogin(), correctAccount.getPassword());
        folderName = cloudService.createFolderAndGetName();
        CloudFile cloudFile = new CloudFileFactory().getValidFile();
        cloudService.uploadFile(cloudFile);
        cloudService.drugAndDropFile(cloudFile);
        cloudService.clickOnMoveConfirmationAndOpenFolder();
        Assert.assertTrue(cloudService.isFileDisplayedInFolder(cloudFile));
    }

    @Test(description = "Get link for a folder and open link")
    public void linkCloudTest() {
        authenticationService.loginCloud(correctAccount.getLogin(), correctAccount.getPassword());
        folderName = cloudService.createFolderAndGetName();
        cloudService.getLinkAndOpenLink();
        Assert.assertEquals(cloudService.getTitleText(), folderName);
    }
}
