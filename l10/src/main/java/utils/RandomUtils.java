package utils;

import org.openqa.selenium.WebDriver;
import pages.MainCloudPage;

import java.io.File;
import java.io.IOException;
import java.util.Random;

public class RandomUtils {

    private static final int BOUND = 5;

    public static File createFile() {
        final Random random = new Random();
        File file = new File("to_upload" + String.valueOf(random.nextInt()) + ".txt");
        try {
            file.createNewFile();
            org.apache.commons.io.FileUtils.writeStringToFile(file, "file text" + new Random().nextInt(BOUND));
        } catch (IOException e) {
            throw new RuntimeException("Error writing file " + file.getName());
        }
        return file;
    }

    public static String createFolder(WebDriver driver) {
        final Random random = new Random();
        String folderName = String.valueOf(random.nextInt());
        MainCloudPage mainCloudPage = new MainCloudPage(driver);
        mainCloudPage.clickOnCreateButton(driver)
                .clickOnCreateFolderButton(driver)
                .enterNameOfNewFolder(driver, folderName)
                .confirmNameOfNewFolder(driver);
        return folderName;
    }
}
