package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    private static final int WAIT_TIME_OUT = 10;

    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public static void highlightingClick(WebDriver driver, By locator) {
        WebElement element = driver.findElement(locator);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].style.border='5px solid red'", element);
        driver.findElement(locator).click();
    }

    public static void waitForAppear(WebDriver driver, By locator) {
        WebDriverWait waitForOne = new WebDriverWait(driver, WAIT_TIME_OUT);
        waitForOne.until(ExpectedConditions.presenceOfElementLocated(locator));
    }
}
