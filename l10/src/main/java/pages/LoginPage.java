package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {

    private static final By ENTER_TO_CLOUD_BUTTON_LOCATOR = By.xpath(
            "//div[@class='try__btn']/input[@class='nav-inner-col-try__bt']");
    private static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@name='Login']");
    private static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@name='Password']");
    private static final By SUBMIT_LOGIN_BUTTON_LOCATOR = By.xpath(
            "//input[@class='x-ph__button__input' and @type='submit']");
    private static final By CLOUD_CONTEXT_LOCATOR = By.xpath("//*[@id='content']");
    private static final By INVALID_MESSAGE_LOCATOR = By.xpath("//div[@id='auth-form']");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public LoginPage clickOnEnterToClodButton() {
        waitForAppear(driver, ENTER_TO_CLOUD_BUTTON_LOCATOR);
        highlightingClick(driver, ENTER_TO_CLOUD_BUTTON_LOCATOR);
        return this;
    }

    public LoginPage setLogin(String login) {
        waitForAppear(driver, LOGIN_INPUT_LOCATOR);
        driver.findElement(LOGIN_INPUT_LOCATOR).sendKeys(login);
        return this;
    }

    public LoginPage setPassword(String login) {
        driver.findElement(PASSWORD_INPUT_LOCATOR).sendKeys(login);
        return this;
    }

    public LoginPage clickOnSubmitButton() {
        highlightingClick(driver, SUBMIT_LOGIN_BUTTON_LOCATOR);
        return this;
    }

    public boolean isAccountButtonPresent() {
        waitForAppear(driver, CLOUD_CONTEXT_LOCATOR);
        return driver.findElement(CLOUD_CONTEXT_LOCATOR).isDisplayed();
    }

    public boolean isInvalidMessagePresent() {
        waitForAppear(driver, INVALID_MESSAGE_LOCATOR);
        driver.findElement(INVALID_MESSAGE_LOCATOR).getText();
        return driver.findElement(INVALID_MESSAGE_LOCATOR).isDisplayed();
    }

    public boolean unsuccessfulLogin() {
        waitForAppear(driver, SUBMIT_LOGIN_BUTTON_LOCATOR);
        return !driver.findElement(SUBMIT_LOGIN_BUTTON_LOCATOR).isDisplayed();
    }
}
