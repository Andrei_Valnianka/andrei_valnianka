package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CloudPublicPage {

    private static final By NAME_OF_ELEMENT_LOCATOR = By.xpath("//span[@class='breadcrumbs__item__plain-header']");

    public String getTitleElementText(WebDriver driver) {
        return driver.findElement(NAME_OF_ELEMENT_LOCATOR).getText();
    }
}
