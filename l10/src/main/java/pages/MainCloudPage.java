package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.io.File;
import java.io.IOException;

public class MainCloudPage extends BasePage {

    public MainCloudPage(WebDriver driver) {
        super(driver);
    }

    private static final String URL_OF_MAIN_CLOUD_PAGE = "https://cloud.mail.ru/home/";
    private static final By ROOT_FOLDER_LOCATOR = By.xpath(
            "//span[@class='breadcrumbs__item__plain-header' and text()='Облако']");
    private static final By CREATE_BUTTON_LOCATOR = By.xpath(
            "//div[@id='toolbar-left']//div[@class='b-dropdown b-dropdown_create-document-dropdown']");
    private static final By CREATE_FOLDER_LOCATOR = By.xpath(
            "//div[@id='toolbar-left']//*[@data-name='folder']");
    private static final By SUBMIT_NAME_OF_NEW_FOLDER_LOCATOR = By.xpath(
            "//div[@class='b-layer__controls__buttons']/*[@data-name='add']");
    private static final By INPUT_NAME_OF_NEW_FOLDER_LOCATOR = By.xpath("//*[@class='layer__input']");
    private static final By DELETE_BUTTON_LOCATOR = By.xpath(
            "//div[@data-name='remove' and not (contains (@aria-disabled,'disabled'))]");
    private static final By REMOVE_CONFIRMATION_BUTTON_LOCATOR = By.xpath(
            "//div[@class='layer_remove']//button[@data-name='remove']");
    private static final By ADD_BUTTON_LOCATOR = By.xpath(
            "//*[@class='b-sticky' and not (contains(@style, 'hidden'))]//"
                    + "div[@data-name='upload' and not (contains(@class, 'drop-zone'))]");
    private static final By DROP_ZONE_LOCATOR = By.xpath(
            "//div[@class='drop-zone']/input[@type='file']");
    private static final By SHARE_LINK_BUTTON_IN_CONTEXT_MENU_LOCATOR = By.xpath(
            "//*[@data-name='publish']");
    private static final By SHARE_LINK_CONFIRMATION_BUTTON_LOCATOR = By.xpath(
            "//button[@data-name='copy']");
    private static final By URL_INPUT_LOCATOR = By.xpath(
            "//div[@class='publishing__url']/input[@data-name='url']");
    private static final By MOVE_CONFIRMATION_BUTTON_LOCATOR = By.xpath("//button[@data-name='move']");
    private static final String PATTERN_FOR_FOLDER_SEARCH = "//div[@class='b-filename__text']/*["
            + "contains (text(), '%s')]";
    private static final String PATTERN_FOR_FILE_SEARCH = "//div[@data-name='link' and @data-id='/%s']";
    private static final String PATTERN_FOR_SEARCH_FILE_IN_FOLDER = "//div[@data-id='/%s/%s']";
    private static final String PATTERN_FOR_MARK_ELEMENT = "//div[@data-id='/%s']//div[@data-bem='b-checkbox']";
    private static final String PATTERN_FOR_SEARCH_WEB_ELEMENT = "//div[@class='b-filename__text']/*["
            + "contains (text(), '%s')]";
    private static final String PATTERN_FOR_CONTEXT_MENU = "//div[@class='b-filename__text']/*["
            + "contains (text(), '%s')]";

    public MainCloudPage clickOnCreateButton(WebDriver driver) {
        highlightingClick(driver, CREATE_BUTTON_LOCATOR);
        return this;
    }

    public MainCloudPage clickOnCreateFolderButton(WebDriver driver) {
        highlightingClick(driver, CREATE_FOLDER_LOCATOR);
        return this;
    }

    public MainCloudPage enterNameOfNewFolder(WebDriver driver, String folderName) {
        driver.findElement(INPUT_NAME_OF_NEW_FOLDER_LOCATOR).sendKeys(folderName);
        return this;
    }

    public MainCloudPage confirmNameOfNewFolder(WebDriver driver) {
        highlightingClick(driver, SUBMIT_NAME_OF_NEW_FOLDER_LOCATOR);
        return this;
    }

    public WebElement getWebElement(String filerName) {
        return driver.findElement(By.xpath(String.format(PATTERN_FOR_FILE_SEARCH, filerName)));
    }

    public boolean isUploadFileDisplayed(WebDriver driver, String filerName) {
        waitForAppear(driver, By.xpath(String.format(PATTERN_FOR_FILE_SEARCH, filerName)));
        try {
            return driver.findElement(By.xpath(String.format(PATTERN_FOR_FILE_SEARCH, filerName))).isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean isFolderPresentInRootFolder(WebDriver driver, String folderName) {
        waitForAppear(driver, ROOT_FOLDER_LOCATOR);
        try {
            return driver.findElement(By.xpath(String.format(PATTERN_FOR_FOLDER_SEARCH, folderName))).isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean isFilePresentInFolder(WebDriver driver, String fileName, String folderName) {
        try {
            return driver.findElement(By.xpath(String.format(
                    PATTERN_FOR_SEARCH_FILE_IN_FOLDER, folderName, fileName))).isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public MainCloudPage markTheElement(WebDriver driver, String folderName) {
        waitForAppear(driver, By.xpath(String.format(PATTERN_FOR_MARK_ELEMENT, folderName)));
        driver.findElement(By.xpath(String.format(PATTERN_FOR_MARK_ELEMENT, folderName))).click();
        return this;
    }

    public MainCloudPage clickOnDeleteButton(WebDriver driver) {
        highlightingClick(driver, DELETE_BUTTON_LOCATOR);
        return this;
    }

    public MainCloudPage clickOnRemoveConfirmationButton(WebDriver driver) {
        waitForAppear(driver, REMOVE_CONFIRMATION_BUTTON_LOCATOR);
        highlightingClick(driver, REMOVE_CONFIRMATION_BUTTON_LOCATOR);
        return this;
    }

    public MainCloudPage clickOnAddButton(WebDriver driver) {
        highlightingClick(driver, ADD_BUTTON_LOCATOR);
        return this;
    }

    public MainCloudPage setPathInDropZone(WebDriver driver, File file) throws IOException {
        driver.findElement(DROP_ZONE_LOCATOR).sendKeys(file.getAbsolutePath());
        return this;
    }

    public MainCloudPage clickOnWebElement(WebDriver driver, String elementName) {
        By locator = By.xpath(String.format(PATTERN_FOR_SEARCH_WEB_ELEMENT, elementName));
        highlightingClick(driver, locator);
        return this;
    }

    public MainCloudPage clickMoveConfirmationButton(WebDriver driver) {
        waitForAppear(driver, MOVE_CONFIRMATION_BUTTON_LOCATOR);
        highlightingClick(driver, MOVE_CONFIRMATION_BUTTON_LOCATOR);
        return this;
    }

    public MainCloudPage contextClick(WebDriver driver, String elementName) {
        WebElement element = driver.findElement(By.xpath(String.format(PATTERN_FOR_CONTEXT_MENU, elementName)));
        Actions builder = new Actions(driver);
        builder.moveToElement(element).contextClick().build().perform();
        return this;
    }

    public MainCloudPage clickOnShareLinkInContextMenu(WebDriver driver) {
        highlightingClick(driver, SHARE_LINK_BUTTON_IN_CONTEXT_MENU_LOCATOR);
        return this;
    }

    public MainCloudPage clickOnShareLinkConfirmationButton(WebDriver driver) {
        highlightingClick(driver, SHARE_LINK_CONFIRMATION_BUTTON_LOCATOR);
        return this;
    }

    public String getUrlText(WebDriver driver) {
        return driver.findElement(URL_INPUT_LOCATOR).getAttribute("value");
    }

    public MainCloudPage goToMainCloudPage() {
        driver.navigate().to(URL_OF_MAIN_CLOUD_PAGE);
        return this;
    }
}


