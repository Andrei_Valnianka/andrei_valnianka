package tests;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.CloudPublicPage;
import pages.MainCloudPage;

import java.io.File;
import java.io.IOException;

import static utils.RandomUtils.createFile;
import static utils.RandomUtils.createFolder;

public class FileTests extends BaseTest {

    @Test
    public void createFolderTest() {
        openCloudMailru();
        String folderName = createFolder(driver);
        Assert.assertEquals(new MainCloudPage(driver).isFolderPresentInRootFolder(driver, folderName), true);
    }

    @Test
    public void deleteFolderTest() {
        openCloudMailru();
        String folderName = createFolder(driver);
        MainCloudPage mainCloudPage = new MainCloudPage(driver);
        mainCloudPage.markTheElement(driver, folderName)
                .clickOnDeleteButton(driver)
                .clickOnRemoveConfirmationButton(driver)
                .goToMainCloudPage();
        Assert.assertEquals(mainCloudPage.isFolderPresentInRootFolder(driver, folderName), false);
    }

    @Test
    public void uploadFileTest() throws IOException {
        openCloudMailru();
        File file = createFile();
        String fileName = file.getName();
        MainCloudPage mainCloudPage = new MainCloudPage(driver);
        mainCloudPage.clickOnAddButton(driver)
                .setPathInDropZone(driver, file);
        Assert.assertEquals(mainCloudPage.isUploadFileDisplayed(driver, fileName), true);
    }

    @Test
    public void dragAndDropTest() throws IOException {
        openCloudMailru();
        MainCloudPage mainCloudPage = new MainCloudPage(driver);
        String folderName = createFolder(driver);
        File file = createFile();
        String fileName = file.getName();
        mainCloudPage.clickOnAddButton(driver)
                .setPathInDropZone(driver, file);
        WebElement target = mainCloudPage.getWebElement(folderName);
        WebElement source = mainCloudPage.getWebElement(fileName);
        Actions builder = new Actions(driver);
        builder.dragAndDrop(source, target).perform();
        mainCloudPage.clickMoveConfirmationButton(driver)
                .clickOnWebElement(driver, folderName);
        Assert.assertEquals(mainCloudPage.isFilePresentInFolder(driver, fileName, folderName), true);
    }

    @Test
    public void linkCloudTest() {
        openCloudMailru();
        MainCloudPage mainCloudPage = new MainCloudPage(driver);
        String folderName = createFolder(driver);
        mainCloudPage.contextClick(driver, folderName)
                .clickOnShareLinkInContextMenu(driver)
                .clickOnShareLinkConfirmationButton(driver);
        String link = mainCloudPage.getUrlText(driver);
        driver.navigate().to(link);
        Assert.assertEquals(new CloudPublicPage().getTitleElementText(driver), folderName);
    }
}
