package tests;

import pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {

    @DataProvider(name = "Data for login test")
    public Object[][] dataForLoginTest() {
        return new Object[][]{
                {"vasil_pupkin2018@mail.ru", "ekscentrisitet2018", true},
                {"vasil_pupkin2018@mail.ru", "ekscentrisitet2018", true}
        };
    }

    @Test(dataProvider = "Data for login test")
    public void loginTest(String login, String password, boolean expectedResult) {
        tryToOpen(login, password);
        Assert.assertEquals(new LoginPage(driver).isAccountButtonPresent(), expectedResult);
    }

    @DataProvider(name = "Data login with empty field test")
    public Object[][] dataForLoginWithEmptyFieldTest() {
        return new Object[][]{
                {"vasil_pupkin2018@mail.ru", ""},
                {"", "ekscentrisitet2018"}
        };
    }

    @Test(dataProvider = "Data login with empty field test")
    public void loginWithEmptyFieldTest(String login, String password) {
        tryToOpen(login, password);
        Assert.assertEquals(new LoginPage(driver).unsuccessfulLogin(), true);
    }

    @DataProvider(name = "Data for invalid login test")
    public Object[][] dataForInvalidLoginTest() {
        return new Object[][]{
            {"vasil_pupkin2018@mail.ru", "abyrvalg", true}
        };
    }

    @Test(dataProvider = "Data for invalid login test")
    public void invalidLoginTest(String login, String password, boolean expectedResult) {
        tryToOpen(login, password);
        Assert.assertEquals(new LoginPage(driver).isInvalidMessagePresent(), expectedResult);
    }
}
