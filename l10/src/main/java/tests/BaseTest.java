package tests;

import pages.LoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    private static final String CORRECT_EMAIL = "vasil_pupkin2018@mail.ru";
    private static final String CORRECT_PASSWORD = "ekscentrisitet2018";
    private static final int IMPLICITLY_WAIT = 4;
    private static final int IMPLICITLY_PAGE_WAIT = 20;
    private static final int IMPLICITLY_SCRIPT_WAIT = 20;

    protected WebDriver driver;

    @BeforeMethod
    public void setUpDriver() {
        System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(IMPLICITLY_PAGE_WAIT, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(IMPLICITLY_SCRIPT_WAIT, TimeUnit.SECONDS);
        driver.get("http://cloud.mail.ru");
        driver.manage().window().maximize();
    }

    public void tryToOpen(String login, String password) {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.clickOnEnterToClodButton()
                .setLogin(login)
                .setPassword(password)
                .clickOnSubmitButton();
    }

    public void openCloudMailru() {
        tryToOpen(CORRECT_EMAIL, CORRECT_PASSWORD);
    }

    @AfterMethod
    public void tearDown() {
        driver.close();
        driver.quit();
    }
}
