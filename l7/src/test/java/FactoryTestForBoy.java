import com.epam.gomel.homework.Month;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

import static com.epam.gomel.homework.Month.*;

public class FactoryTestForBoy {
    @DataProvider(name = "Data for boys creation")
    public Object[][] getBoysData() {
        return new Object[][]{
                {JUNE, 1_000_000},
                {JULY, 1_000},
                {FEBRUARY, -100},
                {AUGUST, 500_000},
                {NOVEMBER, 2_000_000}
        };
    }

    @Factory(dataProvider = "Data for boys creation")
    public Object[] getBoysTet(Month birthdayMonth, double wealth) {
        return new Object[]{new TestsForBoy(birthdayMonth, wealth)};
    }
}
