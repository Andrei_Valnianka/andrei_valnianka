import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import com.epam.gomel.homework.Month;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestsForBoy extends BaseTest {

    private Boy boy;
    private Girl girl;
    private Month birthdayMonth;
    private double wealth;

    public TestsForBoy(Month birthdayMonth, double wealth) {
        this.birthdayMonth = birthdayMonth;
        this.wealth = wealth;
    }

    @Test(description = "Test for method isRich", groups = "boy with factory")
    public void testIsRich() {
        boy = new Boy(birthdayMonth, wealth);
        if (boy.getWealth() >= 1_000_000) {
            Assert.assertEquals(boy.isRich(), true, "Invalid money checking in method isRich");
        }
        if (boy.getWealth() < 1_000_000) {
            Assert.assertEquals(boy.isRich(), false, "Invalid money checking in method isRich");
        }
    }

    @Test(description = "Test for method IsPrettyGirlFriend", groups = "boy with factory")
    public void testIsPrettyGirlFriend() {
        girl = new Girl(true);
        boy = new Boy(birthdayMonth, wealth, girl);
        Assert.assertEquals(boy.isPrettyGirlFriend(), true, "Invalid girlfriend");
    }

    @Test(description = "Test for method IsPrettyGirlFriend with no girlfriend", groups = "boy with factory")
    public void testIsPrettyGirlFriendWithNoPrettyGirl() {
        girl = new Girl(false);
        boy = new Boy(birthdayMonth, wealth, girl);
        Assert.assertEquals(boy.isPrettyGirlFriend(), false, "Invalid girlfriend");
    }

    @Test(description = "Test for method IsPrettyGirlFriend with no girl", groups = {"boy with factory", "negative"})
    public void testIsPrettyGirlFriendWithNoGirlFriend() {
        girl = null;
        boy = new Boy(birthdayMonth, wealth, girl);
        Assert.assertEquals(boy.isPrettyGirlFriend(), false, "Invalid girlfriend");
    }

    @Test(description = "Test for method IsSummerMonth", groups = "boy with factory")
    public void testIsSummerMonth() {
        boy = new Boy(birthdayMonth);
        switch (birthdayMonth) {
            case JUNE:
                Assert.assertEquals(boy.isSummerMonth(), true, "Invalid birthday month");
                break;
            case JULY:
                Assert.assertEquals(boy.isSummerMonth(), true, "Invalid birthday month");
                break;
            case AUGUST:
                Assert.assertEquals(boy.isSummerMonth(), true, "Invalid birthday month");
                break;
            default:
                Assert.assertEquals(boy.isSummerMonth(), false, "Invalid birthday month");
        }
    }

    @Test(description = "Test for method spendSomeMoney with exception",
            expectedExceptions = RuntimeException.class,
            expectedExceptionsMessageRegExp = "Not enough money! .*",
            groups = {"boy with factory", "negative"})
    public void testSpendSomeMoneyWithException() {
        boy = new Boy(birthdayMonth, wealth, girl);
        double amountForSpending = 2_500_020;
        if (amountForSpending > boy.getWealth()) {
            boy.spendSomeMoney(amountForSpending);
        }
    }

    @Test(description = "Test for correct calculation in method spendSomeMoney", groups = "boy with factory")
    public void testSpendSomeMoneyWithEnoughMoney() {
        girl = new Girl();
        boy = new Boy(birthdayMonth, wealth, girl);
        double amountForSpending = -1_000;
        if (amountForSpending <= boy.getWealth()) {
            double beforeSpending = boy.getWealth();
            boy.spendSomeMoney(amountForSpending);
            double afterSpending = boy.getWealth();
            Assert.assertEquals(afterSpending, beforeSpending - amountForSpending, 0.0, "Some problem with money transaction");
        }
    }

    @Test(description = "Test for method getMood", groups = "boy with factory")
    public void testGetMood() {
        girl = new Girl();
        boy = new Boy(birthdayMonth, wealth, girl);
        switch (boy.getMood()) {
            case EXCELLENT:
                Assert.assertEquals(boy.isRich() & boy.isPrettyGirlFriend() & boy.isSummerMonth(), true, "Invalid mood");
                break;
            case GOOD:
                Assert.assertEquals(boy.isRich() & boy.isPrettyGirlFriend(), true, "Invalid mood");
                break;
            case NEUTRAL:
                Assert.assertEquals(boy.isRich() & boy.isSummerMonth(), true, "Invalid mood");
                break;
            case BAD:
                Assert.assertEquals(boy.isRich() || boy.isPrettyGirlFriend() || boy.isSummerMonth(), true, "Invalid mood");
                break;
            default:
                Assert.assertEquals(boy.isRich() || boy.isPrettyGirlFriend() || boy.isSummerMonth(), false, "Invalid mood");
        }
    }

    @Test(description = "Test for method getMood with pretty girl", groups = "boy with factory")
    public void testGetMoodForPrettyGirl() {
        girl = new Girl(true);
        boy = new Boy(birthdayMonth, wealth, girl);
        switch (boy.getMood()) {
            case EXCELLENT:
                Assert.assertEquals(boy.isRich() & boy.isPrettyGirlFriend() & boy.isSummerMonth(), true, "Invalid mood");
                break;
            case GOOD:
                Assert.assertEquals(boy.isRich() & boy.isPrettyGirlFriend(), true, "Invalid mood");
                break;
            case NEUTRAL:
                Assert.assertEquals(boy.isRich() & boy.isSummerMonth(), true, "Invalid mood");
                break;
            case BAD:
                Assert.assertEquals(boy.isRich() || boy.isPrettyGirlFriend() || boy.isSummerMonth(), true, "Invalid mood");
                break;
            default:
                Assert.assertEquals(boy.isRich() || boy.isPrettyGirlFriend() || boy.isSummerMonth(), false, "Invalid mood");
        }
    }
}
