import com.epam.gomel.homework.Boy;
import com.epam.gomel.homework.Girl;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import static com.epam.gomel.homework.Month.AUGUST;
import static com.epam.gomel.homework.Mood.*;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class TestsForGirl extends BaseTest {

    private Boy boy;
    private Girl girl;

    @BeforeClass
    public void setGirl() {
        boy = new Boy(AUGUST, 100_001);
        girl = new Girl(false, true, boy);
    }

    public static Matcher<Girl> pretty() {
        return new FeatureMatcher<Girl, Boolean>(equalTo(true), "Girl should be pretty", "Pretty -") {
            @Override
            protected Boolean featureValueOf(Girl girl) {
                return girl.isPretty();
            }
        };
    }

    @Test(description = "Test for girl setter with hamcrest matcher")
    public void testForGirlSetterWithHamcrest() {
        girl = new Girl();
        girl.setPretty(true);
        assertThat(girl, pretty());
    }

    @Test(description = "Test for girl setter with hamcrest matcher")
    public void mockedBoy() {
        Boy mockedBoy = mock(Boy.class);
        girl = new Girl();
        mockedBoy.setGirlFriend(girl);
        Mockito.verify(mockedBoy).setGirlFriend(girl);
    }

    @Test(description = "Test for method isSlimFriendBecameFat for scary girl and fat friend")
    @Parameters({"isPretty", "isSlimFriendGotAFewKilos"})
    public void testIsSlimFriendBecameFatForScaryGirl(boolean isPretty, boolean isSlimFriendGotAFewKilos) {
        girl = new Girl(isPretty, isSlimFriendGotAFewKilos);
        Assert.assertEquals(girl.isSlimFriendBecameFat(), true, "Invalid obesity");
    }

    @Test(description = "Test for method isSlimFriendBecameFat for scary girl and slim friend", priority = 1)
    public void testIsSlimFriendBecameFatForScaryGirlWithSlimFriend() {
        girl = new Girl(false, false);
        Assert.assertEquals(girl.isSlimFriendBecameFat(), false, "Invalid obesity");
    }

    @Test(description = "Test for method isSlimFriendBecameFat for scary girl and slim friend", priority = 2)
    public void testIsSlimFriendBecameFatForPrettyGirlWithFatFriend() {
        girl = new Girl(true, true);
        Assert.assertEquals(girl.isSlimFriendBecameFat(), false, "Invalid obesity");
    }

    @Test(description = "Test for method isSlimFriendBecameFat for scary girl and slim friend", priority = 3)
    public void testIsSlimFriendBecameFatForPrettyGirlWithSlimFriend() {
        girl = new Girl(true, false);
        Assert.assertEquals(girl.isSlimFriendBecameFat(), false, "Invalid obesity");
    }

    @Test(description = "Test for method isBoyFriendWillBuyNewShoes")
    public void testIsBoyFriendWillBuyNewShoes() {
        if (girl.isBoyfriendRich() && girl.isPretty()) {
            Assert.assertEquals(girl.isBoyFriendWillBuyNewShoes(), true, "Some troubles with buying new shoes");
        } else {
            Assert.assertEquals(girl.isBoyFriendWillBuyNewShoes(), false, "Some troubles with buying new shoes");
        }
    }

    @Test(description = "Test for method isBoyfriendRich",
            dependsOnMethods = "testIsSlimFriendBecameFatForPrettyGirlWithFatFriend")
    public void testIsBoyfriendRich() {
        if (boy != null) {
            if (boy.isRich()) {
                Assert.assertEquals(girl.isBoyfriendRich(), true, "Some troubles with wealth");
            } else {
                Assert.assertEquals(girl.isBoyfriendRich(), false, "Some troubles with wealth");
            }
        } else {
            Assert.assertEquals(girl.isBoyfriendRich(), false, "Some troubles with wealth");
        }
    }

    @Test(description = "Test for method spendBoyFriendMoney for rich boy")
    public void testSpendBoyFriendMoneyWithRichBoy() {
        double amountForSpending = 100;
        boy = new Boy(AUGUST, 1_000_000, girl);
        girl.spendBoyFriendMoney(amountForSpending);
        Assert.assertEquals(999_900, boy.getWealth(), 0.0, "Some problem with money transaction");
    }

    @Test(description = "Test for method spendBoyFriendMoney for poor boy")
    public void testSpendBoyFriendMoneyWithPoorBoy() {
        double amountForSpending = 100;
        boy = new Boy(AUGUST, 900_000);
        girl.spendBoyFriendMoney(amountForSpending);
        Assert.assertEquals(boy.getWealth(), 900_000, 0.0, "Some problem with money transaction");
    }

    @Test(description = "Test for getMood with EXCELLENT mood")
    public void testGetMoodWithExcellentMood() {
        boy = new Boy(AUGUST, 2_000_000);
        girl = new Girl(true, false, boy);
        Assert.assertEquals(girl.getMood(), EXCELLENT, "Invalid mood");
    }

    @Test(description = "Test for getMood with GOOD mood for pretty girl")
    public void testGetMoodWithGoodMoodForPrettyGirl() {
        boy = new Boy(AUGUST, 100_000);
        girl = new Girl(false, false, boy);
        girl.setPretty(true);
        Assert.assertEquals(girl.getMood(), GOOD, "Invalid mood");
    }

    @Test(description = "Test for getMood with GOOD mood for rich boy")
    public void testGetMoodWithGoodForRichBoy() {
        boy = new Boy(AUGUST, 1_000_000);
        girl = new Girl(false, false, boy);
        Assert.assertEquals(girl.getMood(), GOOD, "Invalid mood");
    }

    @Test(description = "Test for getMood with NEUTRAL mood")
    public void testGetMoodWithNeutral() {
        girl = new Girl(false, true);
        Assert.assertEquals(girl.getMood(), NEUTRAL, "Invalid mood");
    }

    @Test(description = "Test for getMood with hate mood")
    public void testGetMoodWithHate() {
        girl = new Girl(false, false);
        Assert.assertEquals(girl.getMood(), I_HATE_THEM_ALL, "Invalid mood");
    }
}


