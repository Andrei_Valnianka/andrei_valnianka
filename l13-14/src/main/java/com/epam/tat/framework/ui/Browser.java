package com.epam.tat.framework.ui;

import com.epam.tat.framework.logging.Log;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;

public final class Browser implements WrapsDriver {

    private static final int WAIT_TIME_OUT = 10;

    private static ThreadLocal<Browser> instance = new ThreadLocal<>();
    private WebDriver wrappedWebDriver;

    private Browser() {
        Log.debug("Start browser");
        wrappedWebDriver = WebDriverFactory.getWebDriver();
    }

    public static synchronized Browser getInstance() {
        if (instance.get() == null) {
            instance.set(new Browser());
        }
        return instance.get();
    }

    public void stopBrowser() {
        Log.debug("Stop browser");
        try {
            wrappedWebDriver.close();
            wrappedWebDriver.quit();
        } finally {
            instance.set(null);
        }
    }

    @Override
    public WebDriver getWrappedDriver() {
        Log.debug("Get wrapped webDriver");
        return wrappedWebDriver;
    }

    public Browser getUrlAndMaximize(String url) {
        Log.debug(String.format("Get url: [%s] and maximize window", url));
        wrappedWebDriver.get(url);
        wrappedWebDriver.manage().window().maximize();
        return this;
    }

    public void highlightingElement(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) wrappedWebDriver;
        js.executeScript("arguments[0].style.border='5px solid red'", element);
    }

    public Browser click(By by) {
        Log.debug(String.format("Click on element [%s]", by));
        WebElement element = wrappedWebDriver.findElement(by);
        highlightingElement(element);
        wrappedWebDriver.findElement(by).click();
        return this;
    }

    public Browser click(String pattern, String name) {
        Log.debug(String.format("Click on element [%s]", By.xpath(String.format(pattern, name))));
        WebElement element = wrappedWebDriver.findElement(By.xpath(String.format(pattern, name)));
        highlightingElement(element);
        wrappedWebDriver.findElement(By.xpath(String.format(pattern, name))).click();
        return this;
    }

    public Browser sendKeys(By by, String keys) {
        Log.debug(String.format("Send keys: [%s] in element [%s]", keys, by));
        wrappedWebDriver.findElement(by)
                .sendKeys(keys);
        return this;
    }

    public Browser waitForAppear(By by) {
        Log.debug(String.format("Wait element: [%s], wait timeout [%s]", by, WAIT_TIME_OUT));
        WebDriverWait waitForOne = new WebDriverWait(wrappedWebDriver, WAIT_TIME_OUT);
        waitForOne.until(ExpectedConditions.visibilityOfElementLocated(by));
        return this;
    }

    public Browser waitForAppear(String pattern, String name) {
        Log.debug(String.format("Wait element: [%s], wait timeout [%s]",
                By.xpath(String.format(pattern, name)), WAIT_TIME_OUT));
        new WebDriverWait(wrappedWebDriver, WAIT_TIME_OUT)
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(pattern, name))));
        return this;
    }

    public String getText(By by) {
        Log.debug(String.format("Get text from element [%s]", by));
        return wrappedWebDriver.findElement(by).getText();
    }

    public boolean isDisplayed(By by) {
        Log.debug(String.format("Is element: [%s] displayed", by));
        return wrappedWebDriver.findElement(by).isDisplayed();
    }

    public boolean isDisplayed(String pattern, String name) {
        Log.debug(String.format("Is element: [%s] displayed", By.xpath(String.format(pattern, name))));
        return wrappedWebDriver.findElement(By.xpath(String.format(pattern, name))).isDisplayed();
    }

    public boolean isDisplayed(String pattern, String folderName, String fileName) {
        Log.debug(String.format("Is element: [%s] displayed", By.xpath(String.format(pattern, folderName, fileName))));
        return wrappedWebDriver.findElement(By.xpath(String.format(pattern, folderName, fileName))).isDisplayed();
    }

    public String getAttribute(By by, String attributeName) {
        Log.debug(String.format("Get attribute: [%s], with name [%s]", by, attributeName));
        return wrappedWebDriver.findElement(by).getAttribute(attributeName);
    }

    public Browser navigate(String url) {
        Log.debug(String.format("Navigate to url [%s]", url));
        wrappedWebDriver.navigate().to(url);
        getInstance().screenshot();
        return this;
    }

    public WebElement getWebElement(String pattern, String name) {
        Log.debug(String.format("Get webElement (file): [%s]", By.xpath(String.format(pattern, name))));
        return wrappedWebDriver.findElement(By.xpath(String.format(pattern, name)));
    }

    public Browser dragAndDropAction(WebElement target, WebElement source) {
        Log.debug(String.format("Get file: [%s] and drop to folder: [%s]", source, target));
        Actions builder = new Actions(getWrappedDriver());
        builder.dragAndDrop(source, target).perform();
        getInstance().screenshot();
        return this;
    }

    public Browser contextClickAction(WebElement element) {
        Log.debug(String.format("Context click on [%s]", element));
        Actions builder = new Actions(getWrappedDriver());
        builder.moveToElement(element).contextClick().build().perform();
        getInstance().screenshot();
        return this;
    }

    public File screenshot() {
        File screenshotFile = new File("screenshots/" + System.nanoTime() + ".png");
        try {
            byte[] screenshotBytes = ((TakesScreenshot) wrappedWebDriver).getScreenshotAs(OutputType.BYTES);
            FileUtils.writeByteArrayToFile(screenshotFile, screenshotBytes);
            Log.debug(String.format("Screenshot taken file: %s", screenshotFile.getAbsolutePath()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return screenshotFile;
    }

    public void switchToFrame() {
        getWrappedDriver().switchTo().defaultContent();
    }

    public void switchToFrame(int frameIndex) {
        getWrappedDriver().switchTo().frame(frameIndex);
    }
}
