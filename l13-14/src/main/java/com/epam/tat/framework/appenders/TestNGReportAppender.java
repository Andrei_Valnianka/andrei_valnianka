package com.epam.tat.framework.appenders;

import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;
import org.testng.Reporter;

public class TestNGReportAppender extends AppenderSkeleton {

    private static final String SCREENSHOT_MESSAGE = "Screenshot taken file:";
    private static final int PATH_BEGINNING = 23;

    @Override
    protected void append(LoggingEvent event) {
        Reporter.log(layout.format(event));
        if (event.getRenderedMessage().contains(SCREENSHOT_MESSAGE)) {
            setScreenshotInHtml(event);
        }
    }

    @Override
    public void close() {
    }

    @Override
    public boolean requiresLayout() {
        return true;
    }

    public void setScreenshotInHtml(LoggingEvent event) {
        String absolutePath = event.getRenderedMessage().substring(PATH_BEGINNING);
        Reporter.setEscapeHtml(false);
        Reporter.log("<a href='" + absolutePath + "' >" + absolutePath + "</a> ");
        Reporter.log("<img src='" + absolutePath + "' Width=\"800\" Height=\"600\"> ");
    }
}
