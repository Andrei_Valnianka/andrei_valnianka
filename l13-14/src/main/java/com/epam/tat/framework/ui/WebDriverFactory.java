package com.epam.tat.framework.ui;

import com.epam.tat.framework.runner.Parameters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class WebDriverFactory {

    private static final int IMPLICITLY_WAIT = 4;
    private static final int IMPLICITLY_PAGE_WAIT = 20;
    private static final int IMPLICITLY_SCRIPT_WAIT = 20;

    public static WebDriver getWebDriver() {
        WebDriver webDriver;
        switch (Parameters.instance().getBrowserType()) {
            case CHROME:
                System.setProperty("webdriver.chrome.driver", "./src/main/resources/drivers/chromedriver.exe");
                webDriver = new ChromeDriver();
                webDriver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT, TimeUnit.SECONDS);
                webDriver.manage().timeouts().pageLoadTimeout(IMPLICITLY_PAGE_WAIT, TimeUnit.SECONDS);
                webDriver.manage().timeouts().setScriptTimeout(IMPLICITLY_SCRIPT_WAIT, TimeUnit.SECONDS);
                break;
            case FIREFOX:
                System.setProperty("webdriver.firefox.driver", "./src/main/resources/drivers/chromedriver.exe");
                webDriver = new FirefoxDriver();
                break;
            default:
                throw new RuntimeException("No support ");
        }
        return webDriver;
    }
}
