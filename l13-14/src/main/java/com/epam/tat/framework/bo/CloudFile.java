package com.epam.tat.framework.bo;

import java.io.File;

public class CloudFile {

    private File file;

    public CloudFile(File file) {
        this.file = file;
    }

    public String getFileName() {
        return file.getName();
    }

    public String getFilePath() {
        return file.getAbsolutePath();
    }
}
