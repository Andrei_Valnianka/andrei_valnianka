package com.epam.tat.framework.bo;

import static com.epam.tat.framework.utils.RandomUtils.getRandomFile;

public class CloudFileFactory {

    public CloudFile getValidFile() {
        return new CloudFile(getRandomFile());
    }
}
