package com.epam.tat.framework.bo;

public class AccountFactory {

    private static final String CORRECT_EMAIL = "vasil_pupkin2018@mail.ru";
    private static final String CORRECT_PASSWORD = "ekscentrisitet2018";
    private static final String INCORRECT_PASSWORD = "abyrvalg";
    private static final String EMPTY_STRING = "";

    public Account getCorrectAccount() {
        return new Account(CORRECT_EMAIL, CORRECT_PASSWORD);
    }

    public Account getAccountWithoutLogin() {
        Account account = new Account(CORRECT_EMAIL, CORRECT_PASSWORD);
        account.setLogin(EMPTY_STRING);
        return account;
    }

    public Account getAccountWithoutPassword() {
        Account account = new Account(CORRECT_EMAIL, CORRECT_PASSWORD);
        account.setPassword(EMPTY_STRING);
        return account;
    }

    public Account getAccountWithoutLoginAndPassword() {
        Account account = new Account(CORRECT_EMAIL, CORRECT_PASSWORD);
        account.setLogin(EMPTY_STRING);
        account.setPassword(EMPTY_STRING);
        return account;
    }

    public Account getAccountWithoutIncorrectPassword() {
        Account account = new Account(CORRECT_EMAIL, CORRECT_PASSWORD);
        account.setPassword(INCORRECT_PASSWORD);
        return account;
    }
}
