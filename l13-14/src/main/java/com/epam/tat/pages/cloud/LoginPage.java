package com.epam.tat.pages.cloud;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.ui.Element;
import org.openqa.selenium.By;

public class LoginPage {

    private static final By SUBMIT_LOGIN_BUTTON_LOCATOR = By.xpath(
            "//input[@class='x-ph__button__input' and @type='submit']");
    private final Element enterToCloudButton = new Element(By.xpath(
            "//div[@class='try__btn']/input[@class='nav-inner-col-try__bt']"));
    private final Element loginInput = new Element(By.xpath("//input[@name='Login']"));
    private final Element passwordInput = new Element(By.xpath("//input[@name='Password']"));
    private final Element invalidMessage = new Element(By.xpath("//div[@id='auth-form']"));
    private final Element textInvalidLoginMessage = new Element(By.xpath("//div[@id='x-ph__authForm']"
            + "//div[@class='x-ph__form__message js-text']"));
    private final Element cloudLogin = new Element(By.xpath("//i[@id='PH_user-email']"));

    public LoginPage clickOnEnterToClodButton() {
        enterToCloudButton.click();
        return this;
    }

    public LoginPage setLogin(String login) {
        loginInput.sendKeys(login);
        return this;
    }

    public LoginPage setPassword(String password) {
        passwordInput.sendKeys(password);
        return this;
    }

    public void clickOnSubmitButton() {
        Browser.getInstance()
                .click(SUBMIT_LOGIN_BUTTON_LOCATOR);
    }

    public String getLoginText() {
        return cloudLogin.getText();
    }

    public boolean isInvalidMessagePresent() {
        return invalidMessage.isDisplayed();
    }

    public String getTextInvalidLoginMessage() {
        return textInvalidLoginMessage.getText();
    }
}

