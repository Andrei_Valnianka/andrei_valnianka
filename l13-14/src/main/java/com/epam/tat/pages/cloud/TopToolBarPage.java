package com.epam.tat.pages.cloud;

import com.epam.tat.framework.ui.Element;
import org.openqa.selenium.By;

public class TopToolBarPage {

    private final Element createButton = new Element(By.xpath(
            "//div[@id='toolbar-left']//div[@class='b-dropdown b-dropdown_create-document-dropdown']"));
    private final Element createFolder = new Element(By.xpath(
            "//div[@id='toolbar-left']//*[@data-name='folder']"));
    private final Element inputNameOfNewFolder = new Element(By.xpath("//*[@class='layer__input']"));
    private final Element submitNameOfNewFolder = new Element(By.xpath(
            "//div[@class='b-layer__controls__buttons']/*[@data-name='add']"));
    private final Element deleteButton = new Element(By.xpath(
            "//div[@data-name='remove' and not (contains (@aria-disabled,'disabled'))]"));
    private final Element removeConfirmationButton = new Element(By.xpath(
            "//div[@class='layer_remove']//button[@data-name='remove']"));
    private final Element addButton = new Element(By.xpath(
            "//*[@class='b-sticky' and not (contains(@style, 'hidden'))]//"
                    + "div[@data-name='upload' and not (contains(@class, 'drop-zone'))]"));
    private final Element moveConfirmationButton = new Element(By.xpath(
            "//div[@class='b-layer__container']//button[@data-name='move']"));
    private final Element moveConfirmationMessage = new Element(By.xpath(
            "//div[@class='layer_move-confirm']"));

    public TopToolBarPage clickOnCreateButton() {
        createButton.click();
        return this;
    }

    public TopToolBarPage clickOnCreateFolderButton() {
        createFolder.click();
        return this;
    }

    public TopToolBarPage enterNameOfNewFolder(String folderName) {
        inputNameOfNewFolder.sendKeys(folderName);
        return this;
    }

    public TopToolBarPage confirmNameOfNewFolder() {
        submitNameOfNewFolder.click();
        return this;
    }

    public TopToolBarPage clickOnDeleteButton() {
        deleteButton.click();
        return this;
    }

    public TopToolBarPage clickOnRemoveConfirmationButton() {
        removeConfirmationButton.click();
        return this;
    }

    public TopToolBarPage clickOnAddButton() {
        addButton.click();
        return this;
    }

    public TopToolBarPage clickMoveConfirmationButton() {
        moveConfirmationMessage.waitForAppear();
        moveConfirmationButton.click();
        return this;
    }
}
