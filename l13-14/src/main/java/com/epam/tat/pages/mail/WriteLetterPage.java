package com.epam.tat.pages.mail;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.ui.Element;
import org.openqa.selenium.By;

public class WriteLetterPage {

    private static final int FRAME_INDEX = 0;
    private final Element inputForAddress = new Element(By.xpath("//textarea[@data-original-name='To']"));
    private final Element inputForSubject = new Element(By.xpath("//input[@name='Subject']"));
    private final Element inputForText = new Element(By.xpath("//body[@id='tinymce']"));
    private final Element sendButton = new Element(By.xpath("//*[@data-name='send']"));
    private final Element saveDraftButton = new Element(By.xpath(
            " //a[@data-name='saveDraft']"));
    private final Element sendEmailWithoutSubjectConfirmationButton = new Element(By.xpath(
            "//div[@class='is-compose-empty_in']//button[@type='submit']"));
    private final Element saveDropDownMenuButton = new Element(By.xpath("//div[@data-group='save-more']"));
    private final Element messageSaveInDraft = new Element(By.xpath("//a[@href='/messages/drafts']"));

    public WriteLetterPage addressInput(String email) {
        inputForAddress.sendKeys(email);
        return this;
    }

    public WriteLetterPage subjectInput(String letterSubject) {
        inputForSubject.sendKeys(letterSubject);
        return this;
    }

    public WriteLetterPage letterTextInput(String letterText) {
        Browser.getInstance().switchToFrame(FRAME_INDEX);
        inputForText.sendKeys(letterText);
        Browser.getInstance().switchToFrame();
        return this;
    }

    public WriteLetterPage clickOnSendButton() {
        sendButton.click();
        return this;
    }

    public WriteLetterPage clickOnSaveDraftButton() {
        saveDraftButton.click();
        return this;
    }

    public String getTextOfInvalidEmailAlert() {
        return Browser.getInstance().getWrappedDriver().switchTo().alert().getText();
    }

    public void clickOnSendEmailWithoutSubjectAndBody() {
        sendEmailWithoutSubjectConfirmationButton.click();
    }

    public WriteLetterPage clickOnSaveDropDownMenu() {
        saveDropDownMenuButton.click();
        return this;
    }

    public boolean isMessageSaveInDraftDisplayed() {
        return messageSaveInDraft.isDisplayed();
    }
}
