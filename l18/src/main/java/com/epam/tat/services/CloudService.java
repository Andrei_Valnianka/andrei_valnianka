package com.epam.tat.services;

import com.epam.tat.framework.bo.CloudFile;
import com.epam.tat.framework.logging.Log;
import com.epam.tat.pages.cloud.CloudPublicPage;
import com.epam.tat.pages.cloud.LoginPage;
import com.epam.tat.pages.cloud.MainCloudPage;
import com.epam.tat.pages.cloud.TopToolBarPage;

import java.net.MalformedURLException;

public class CloudService {

    LoginPage loginPage = new LoginPage();
    MainCloudPage mainCloudPage = new MainCloudPage();
    TopToolBarPage topToolBarPage = new TopToolBarPage();

    public String getCloudLoginText() throws MalformedURLException, InterruptedException {
        Log.info("Get login");
        return loginPage.getLoginText();
    }

    public String getTextOfInvalidLoginMessage() throws MalformedURLException, InterruptedException {
        Log.info("Get text of invalid login message");
        return new LoginPage().getTextInvalidLoginMessage();
    }

    public boolean isInvalidAlertDisplayed() throws MalformedURLException, InterruptedException {
        Log.info("Is invalid alert displayed");
        return loginPage.isInvalidMessagePresent();
    }

    public void getNewFolder(String folderName) throws MalformedURLException, InterruptedException {
        Log.info(String.format("Create folder with name: [%s]", folderName));
        topToolBarPage.clickOnCreateButton()
                .clickOnCreateFolderButton()
                .enterNameOfNewFolder(folderName)
                .confirmNameOfNewFolder();
    }

    public boolean isFolderDisplayed(String folderName) throws MalformedURLException, InterruptedException {
        Log.info(String.format("Is element: [%s] displayed", folderName));
        return mainCloudPage.isFolderPresentInRootFolder(folderName);
    }

    public boolean isFolderDisplayedInRoot(String folderName) throws MalformedURLException, InterruptedException {
        Log.info(String.format("Is folder: [%s] displayed in root", folderName));
        return mainCloudPage.isFolderPresentInRootFolder(folderName);
    }

    public void deleteFolder(String folderName) throws MalformedURLException, InterruptedException {
        Log.info(String.format("Delete folder: [%s]", folderName));
        mainCloudPage.markTheElement(folderName);
        topToolBarPage.clickOnDeleteButton()
                .clickOnRemoveConfirmationButton();
        mainCloudPage.goToMainCloudPage();
    }

    public void uploadFile(CloudFile cloudFile) throws MalformedURLException, InterruptedException {
        Log.info(String.format("Upload file: [%s]", cloudFile));
        new TopToolBarPage().clickOnAddButton();
        mainCloudPage.setPathInDropZone(cloudFile.getFilePath());
    }

    public boolean isUploadFileDisplayed(CloudFile cloudFile) throws MalformedURLException, InterruptedException {
        Log.info(String.format("Is upload file: [%s] displayed", cloudFile));
        return mainCloudPage.isUploadFileDisplayed(cloudFile.getFileName());
    }

    public void dragAndDropFile(CloudFile cloudFile, String folderName)
            throws MalformedURLException, InterruptedException {
        Log.info(String.format("Get file: [%s] and drop to folder [%s]", cloudFile.getFileName(), folderName));
        mainCloudPage.dragAndDrop(cloudFile.getFileName(), folderName);
    }

    public void clickOnMoveConfirmationAndOpenFolder(String folderName)
            throws MalformedURLException, InterruptedException {
        Log.info(String.format("Click on move confirmation and open folder: [%s]", folderName));
        topToolBarPage.clickMoveConfirmationButton();
        mainCloudPage.clickOnWebElement(folderName);
    }

    public boolean isFileDisplayedInFolder(CloudFile cloudFile, String folderName)
            throws MalformedURLException, InterruptedException {
        Log.info(String.format("Is file: [%s] displayed in folder: [%s]", cloudFile, folderName));
        return mainCloudPage.isFilePresentInFolder(cloudFile.getFileName(), folderName);
    }

    public void getLinkAndOpenLink(String folderName) throws MalformedURLException, InterruptedException {
        Log.info(String.format("Get link to folder: [%s] and open folder by link", folderName));
        mainCloudPage.contextClick(folderName)
                .clickOnShareLinkInContextMenu()
                .clickOnShareLinkConfirmationButton();
        String link = mainCloudPage.getUrlText();
        mainCloudPage.openLink(link);
    }

    public String getTitleText() throws MalformedURLException, InterruptedException {
        Log.info("Get title text");
        return new CloudPublicPage().getTitleElementText();
    }
}
