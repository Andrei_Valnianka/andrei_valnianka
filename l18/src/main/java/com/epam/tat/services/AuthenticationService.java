package com.epam.tat.services;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.pages.cloud.LoginPage;
import com.epam.tat.pages.mail.LoginMailPage;

import java.net.MalformedURLException;

public class AuthenticationService {

    public void loginMail(String email, String password) throws MalformedURLException, InterruptedException {
        Log.info(String.format("Login to mail.ru [%s; %s]", email, password));
        LoginMailPage loginMailPage = new LoginMailPage();
        loginMailPage.inputLogin(email)
                .inputPassword(password)
                .clickSubmitButton();
    }

    public void loginCloud(String login, String password) throws MalformedURLException, InterruptedException {
        Log.info(String.format("Login to cloud.mail.ru [%s; %s]", login, password));
        LoginPage loginPage = new LoginPage();
        loginPage.clickOnEnterToClodButton()
                .setLogin(login)
                .setPassword(password)
                .clickOnSubmitButton();
    }

    public String getMailLoginName() throws MalformedURLException, InterruptedException {
        Log.info("Get login name");
        return new LoginMailPage().getLoginName();
    }

    public String getTextOfInvalidMailMessage() throws MalformedURLException, InterruptedException {
        Log.info("Get text of invalid mail message");
        return new LoginMailPage().getTextOfInvalidMessage();
    }
}
