package com.epam.tat.framework.listeners;

import com.epam.tat.framework.ui.Browser;
import org.testng.IInvokedMethod;
import org.testng.IInvokedMethodListener;
import org.testng.ITestResult;

import java.net.MalformedURLException;

public class TestListener implements IInvokedMethodListener {

    private static final int STATUS_RESULT = 1;

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        System.out.println("[METHOD_STARTED] - " + method.getTestMethod().getMethodName());
    }

    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        System.out.println(String.format("[METHOD_FINISHED] - %s >>> %s",
                method.getTestMethod().getMethodName(), testResult.getStatus() == STATUS_RESULT ? "Success" : "Fail"));
        if (testResult.getStatus() != 1) {
            try {
                Browser.getInstance().screenshot();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}


