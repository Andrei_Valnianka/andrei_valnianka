package com.epam.tat.framework.runner;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.epam.tat.framework.listeners.SuiteListener;
import com.epam.tat.framework.listeners.TestListener;
import com.epam.tat.framework.logging.Log;
import org.testng.TestNG;
import java.util.List;

public class TestRunner {

    public static TestNG configureTestNG() {
        TestNG testNG = new TestNG();
        testNG.addListener(new TestListener());
        testNG.addListener(new SuiteListener());
        List<String> files = Parameters.instance().getSuites();
        testNG.setTestSuites(files);
        return testNG;
    }

    public static void main(String[] args) {
        Log.info("Parse CLI parameters");
        parseCli(args);
        Log.info("Start application");
        configureTestNG().run();
        Log.info("End application");
    }

    private static void parseCli(String[] args) {
        Log.info("Parse CLI by JCommander");
        JCommander jCommander = new JCommander(Parameters.instance());
        try {
            jCommander.parse(args);
        } catch (ParameterException e) {
            Log.error("Parse CLI parameters ERROR", e);
            jCommander.usage();
            System.exit(1);
        }
        if (Parameters.instance().isHelp()) {
            jCommander.usage();
            System.exit(0);
        }
    }
}

