package com.epam.tat.framework.ui;

import com.epam.tat.framework.runner.Parameters;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class WebDriverFactory {

    private static final int IMPLICITLY_WAIT = 4;
    private static final int IMPLICITLY_PAGE_WAIT = 20;
    private static final int IMPLICITLY_SCRIPT_WAIT = 20;

    public static RemoteWebDriver getWebDriver() throws MalformedURLException, InterruptedException {

        RemoteWebDriver webDriver;
        switch (Parameters.instance().getBrowserType()) {
            case CHROME:
                DesiredCapabilities capabilities = DesiredCapabilities.chrome();
                webDriver = new RemoteWebDriver(new URL(Parameters.instance().getNode()), capabilities);
                webDriver.manage().timeouts().implicitlyWait(IMPLICITLY_WAIT, TimeUnit.SECONDS);
                webDriver.manage().timeouts().pageLoadTimeout(IMPLICITLY_PAGE_WAIT, TimeUnit.SECONDS);
                webDriver.manage().timeouts().setScriptTimeout(IMPLICITLY_SCRIPT_WAIT, TimeUnit.SECONDS);
                break;
            case FIREFOX:
                capabilities = DesiredCapabilities.firefox();
                webDriver = new RemoteWebDriver(new URL(Parameters.instance().getNode()), capabilities);
                break;
            default:
                throw new RuntimeException("No support ");
        }
        return webDriver;
    }
}
