package com.epam.tat.framework.ui;

import org.openqa.selenium.By;

import java.net.MalformedURLException;

public class Element {

    private By locator;

    public Element(By locator) {
        this.locator = locator;
    }

    public Element click() throws MalformedURLException, InterruptedException {
        Browser.getInstance().waitForAppear(locator).click(locator);
        return this;
    }

    public String getText() throws MalformedURLException, InterruptedException {
        return Browser.getInstance().waitForAppear(locator).getText(locator);
    }

    public Element sendKeys(String keys) throws MalformedURLException, InterruptedException {
        Browser.getInstance().waitForAppear(locator).sendKeys(locator, keys);
        return this;
    }

    public boolean isDisplayed() throws MalformedURLException, InterruptedException {
        return Browser.getInstance().waitForAppear(locator).isDisplayed(locator);
    }

    public void waitForAppear() throws MalformedURLException, InterruptedException {
        Browser.getInstance().waitForAppear(locator);
    }
}
