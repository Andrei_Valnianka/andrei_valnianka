package com.epam.tat.framework.runner;

import com.beust.jcommander.IStringConverter;
import com.beust.jcommander.Parameter;
import com.epam.tat.framework.ui.BrowserType;
import java.util.ArrayList;
import java.util.List;

public class Parameters {

    private static Parameters instance;

    @Parameter(names = {"--chrome", "-c"}, description = "Path to Google Chrome Driver")
    private String chromeDriver = "./src/main/resources/drivers/chromedriver.exe";

    @Parameter(names = {"--firefox", "-f"}, description = "Path to Gecko Driver")
    private String geckoDriver = "./src/main/resources/drivers/geckodriver.exe";

    @Parameter(names = "--help", help = true, description = "How to use")
    private boolean help;

    @Parameter(names = {"--browser", "-b"}, required = true, description = "Browser type",
            converter = BrowserTypeConverter.class)
    private BrowserType browserType;

    @Parameter(names = {"--suites", "-s"}, description = "Suites to launch")
    private List<String> suite = new ArrayList<String>() {{
            add("./src/main/resources/suites/cloud_tests.xml");
        }};

    @Parameter(names = {"threads", "-t"}, description = "Thread count")
    private int threadCount = 2;

    @Parameter(names = {"node", "-n"}, description = "Node")
    private String node = "http://localhost:4444/wd/hub";

    public static synchronized Parameters instance() {
        if (instance == null) {
            instance = new Parameters();
        }
        return instance;
    }

    public List<String> getSuites() {
        return suite;
    }

    public boolean isHelp() {
        return help;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public String getNode() {
        return node;
    }

    public static class BrowserTypeConverter
            implements IStringConverter<BrowserType> {
        public BrowserType convert(String s) {
            return BrowserType.valueOf(s.toUpperCase());
        }
    }
}
