package com.epam.tat.framework.listeners;

import com.epam.tat.framework.runner.Parameters;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.xml.XmlSuite;

public class SuiteListener implements ISuiteListener {

    @Override
    public void onStart(ISuite suite) {
        suite.getXmlSuite().setParallel(XmlSuite.ParallelMode.METHODS);
        suite.getXmlSuite().setThreadCount(Parameters.instance().getThreadCount());
    }

    @Override
    public void onFinish(ISuite suite) {
    }
}
