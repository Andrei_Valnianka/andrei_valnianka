package com.epam.tat.framework.utils;

import com.epam.tat.framework.logging.Log;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.File;
import java.io.IOException;

public class RandomUtils {

    private static final int RANDOM_BOUND = 5;

    public static File getRandomFile() {
        File file = new File("to_upload" + RandomStringUtils.randomAlphabetic(RANDOM_BOUND) + ".txt");
        try {
            file.createNewFile();
            org.apache.commons.io.FileUtils.writeStringToFile(file, "file text"
                    + RandomStringUtils.randomAlphabetic(RANDOM_BOUND));
        } catch (IOException e) {
            Log.error(String.format("Error writing to file: [%s]", file.getName()));
        }
        return file;
    }

    public static String getRandomName() {
        return RandomStringUtils.randomAlphabetic(RANDOM_BOUND);
    }
}
