package com.epam.tat.tests.cloud;

import com.epam.tat.framework.bo.CloudFile;
import com.epam.tat.framework.bo.CloudFileFactory;
import com.epam.tat.services.CloudService;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;

import static com.epam.tat.framework.utils.RandomUtils.getRandomName;

public class FileTests extends BaseTest {

    private CloudService cloudService = new CloudService();

    @Test(description = "Test for creating a folder")
    public void createFolderTest() throws MalformedURLException, InterruptedException {
        authenticationService.loginCloud(correctAccount.getLogin(), correctAccount.getPassword());
        String folderNameForCreatingTest = getRandomName();
        cloudService.getNewFolder(folderNameForCreatingTest);
        Assert.assertTrue(cloudService.isFolderDisplayed(folderNameForCreatingTest));
    }

    @Test(description = "Test for deleting a folder")
    public void deleteFolderTest() throws MalformedURLException, InterruptedException {
        authenticationService.loginCloud(correctAccount.getLogin(), correctAccount.getPassword());
        String folderNameForDeleteTest = getRandomName();
        cloudService.getNewFolder(folderNameForDeleteTest);
        cloudService.deleteFolder(folderNameForDeleteTest);
        Assert.assertTrue(!cloudService.isFolderDisplayedInRoot(folderNameForDeleteTest));
    }

    @Test(description = "Test for uploading file")
    public void uploadFileTest() throws MalformedURLException, InterruptedException {
        authenticationService.loginCloud(correctAccount.getLogin(), correctAccount.getPassword());
        CloudFile cloudFile = new CloudFileFactory().getValidFile();
        cloudService.uploadFile(cloudFile);
        Assert.assertTrue(cloudService.isUploadFileDisplayed(cloudFile));
    }

    @Test(description = "Drag file and drop in folder")
    public void dragAndDropTest() throws MalformedURLException, InterruptedException {
        authenticationService.loginCloud(correctAccount.getLogin(), correctAccount.getPassword());
        String folderNameForDragAndDropTest = getRandomName();
        cloudService.getNewFolder(folderNameForDragAndDropTest);
        CloudFile cloudFile = new CloudFileFactory().getValidFile();
        cloudService.uploadFile(cloudFile);
        cloudService.dragAndDropFile(cloudFile, folderNameForDragAndDropTest);
        cloudService.clickOnMoveConfirmationAndOpenFolder(folderNameForDragAndDropTest);
        Assert.assertTrue(cloudService.isFileDisplayedInFolder(cloudFile, folderNameForDragAndDropTest));
    }

    @Test(description = "Get link for a folder and open link")
    public void linkCloudTest() throws MalformedURLException, InterruptedException {
        authenticationService.loginCloud(correctAccount.getLogin(), correctAccount.getPassword());
        String folderNameForLinkCloudTest = getRandomName();
        cloudService.getNewFolder(folderNameForLinkCloudTest);
        cloudService.getLinkAndOpenLink(folderNameForLinkCloudTest);
        Assert.assertEquals(cloudService.getTitleText(), folderNameForLinkCloudTest);
    }
}
