package com.epam.tat.tests.mail;

import com.epam.tat.framework.bo.Account;
import com.epam.tat.framework.bo.AccountFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.net.MalformedURLException;

public class LoginTests extends BaseTest {

    @Test(description = "Test for correct login")
    public void correctLoginTest() throws MalformedURLException, InterruptedException {
        authenticationService.loginMail(correctAccount.getLogin(), correctAccount.getPassword());
        Assert.assertEquals(authenticationService.getMailLoginName(), correctAccount.getLogin());
    }

    @DataProvider(name = "Data for negative login test")
    public Object[][] dataForNegativeLoginTest() {
        return new Object[][]{
                {new AccountFactory().getAccountWithoutLogin(), "Введите имя ящика"},
                {new AccountFactory().getAccountWithoutLoginAndPassword(), "Введите имя ящика"},
                {new AccountFactory().getAccountWithoutPassword(), "Введите пароль"}
        };
    }

    @Test(description = "Test for incorrect login", dataProvider = "Data for negative login test")
    public void negativeLoginTest(Account account, String expectedResult)
            throws MalformedURLException, InterruptedException {
        authenticationService.loginMail(account.getLogin(), account.getPassword());
        Assert.assertEquals(authenticationService.getTextOfInvalidMailMessage(), expectedResult);
    }
}
