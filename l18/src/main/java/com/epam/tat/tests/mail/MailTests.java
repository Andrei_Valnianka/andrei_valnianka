package com.epam.tat.tests.mail;

import com.epam.tat.framework.bo.Letter;
import com.epam.tat.framework.bo.LetterFactory;
import com.epam.tat.services.MailService;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.net.MalformedURLException;

public class MailTests extends BaseTest {

    MailService mailService = new MailService();

    @DataProvider(name = "Data for ProperlySend test")
    public Object[][] dataForProperlySendTest() {
        return new Object[][]{
                {new LetterFactory().getLetterWithEmailSubjectBody()}
        };
    }

    @Test(description = "Send letter with subject and body", dataProvider = "Data for ProperlySend test")
    public void properlySendTest(Letter letter) throws MalformedURLException, InterruptedException {
        authenticationService.loginMail(correctAccount.getLogin(), correctAccount.getPassword());
        mailService.sendLetter(letter);
        Assert.assertEquals(mailService.isLetterWithSubjectAndBodyDisplayedInInboxAndOutbox(letter), "");
    }

    @Test(description = "Send letter with invalid email")
    public void improperlySendTest() throws MalformedURLException, InterruptedException {
        authenticationService.loginMail(correctAccount.getLogin(), correctAccount.getPassword());
        mailService.sendInvalidLetter(new LetterFactory().getLetterWithInvalidAddress());
        Assert.assertEquals(mailService.getTextOfInvalidMessage(),
                "В поле «Кому» указан некорректный адрес получателя.\n"
                        + "Исправьте ошибку и отправьте письмо ещё раз.");
    }

    @Test(description = "Send letter without subject and body")
    public void sendMailWithoutSubjectAndBodyTest() throws MalformedURLException, InterruptedException {
        Letter letter = new LetterFactory().getLetterWithoutSubjectAndBody();
        authenticationService.loginMail(correctAccount.getLogin(), correctAccount.getPassword());
        mailService.sendLetterWithoutSubjectAndBody(letter);
        Assert.assertEquals(mailService.isEmptyLetterDisplayedInInboxAndOutbox(), "");
    }

    @Test(description = "Create draft letter and delete it", dataProvider = "Data for ProperlySend test")
    public void draftMailTest(Letter letter) throws MalformedURLException, InterruptedException {
        authenticationService.loginMail(correctAccount.getLogin(), correctAccount.getPassword());
        mailService.saveLetterInDraft(letter);
        mailService.deleteLetterInDraft(letter);
        mailService.deleteLetterInDeleted(letter);
        Assert.assertEquals(mailService.isDeletedLetterDisplayed(letter), "");
    }
}
