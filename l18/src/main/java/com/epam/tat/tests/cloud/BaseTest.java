package com.epam.tat.tests.cloud;

import com.epam.tat.framework.bo.Account;
import com.epam.tat.framework.bo.AccountFactory;
import com.epam.tat.framework.ui.Browser;
import com.epam.tat.services.AuthenticationService;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.net.MalformedURLException;

public class BaseTest {

    Account correctAccount = new AccountFactory().getCorrectAccount();
    AuthenticationService authenticationService = new AuthenticationService();

    @BeforeMethod
    public void setUpDriver() throws MalformedURLException, InterruptedException {
        Browser.getInstance().getUrlAndMaximize("http://cloud.mail.ru");
    }

    @AfterMethod
    public void tearDown() throws MalformedURLException, InterruptedException {
        Browser.getInstance().stopBrowser();
    }
}
