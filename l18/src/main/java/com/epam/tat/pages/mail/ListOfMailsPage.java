package com.epam.tat.pages.mail;

import com.epam.tat.framework.ui.Browser;
import com.epam.tat.framework.ui.Element;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import java.net.MalformedURLException;

public class ListOfMailsPage {

    private static final String MESSAGE_SUBJECT_IN_DELETED_PATTERN =
            "//div[@data-cache-key='500002_undefined_false']//*[@class='b-datalist__item__subj' and text()='%s']";
    private static final String MESSAGE_SUBJECT_IN_INBOX_PATTERN =
            "//div[@data-cache-key='0_undefined_false']//*[@class='b-datalist__item__subj' and text()='%s']";
    private static final String MESSAGE_SUBJECT_IN_OUTBOX_PATTERN =
            "//div[@data-cache-key='500000_undefined_false']//*[@class='b-datalist__item__subj' and text()='%s']";
    private static final String MESSAGE_TEXT_N_INBOX_PATTERN =
            "//div[@data-cache-key='0_undefined_false']//span[@class='b-datalist__item__subj__snippet' "
                    + "and text()='%s']";
    private static final String MESSAGE_TEXT_IN_OUTBOX_PATTERN =
            "//div[@data-cache-key='500000_undefined_false']//span[@class='b-datalist__item__subj__snippet' "
                    + "and text()='%s']";
    private static final String MESSAGE_TEXT_IN_DELETED_PATTERN = "//div[@data-cache-key='500002_undefined_false']"
            + "//span[@class='b-datalist__item__subj__snippet' and text()='%s']";
    private static final String LETTERS_CHECKBOX_PATTERN = "//a[@data-subject='%s']//div[@data-bem='b-checkbox']";
    private Element deleteButton = new Element(By.xpath("//div[@id='b-toolbar__right']//div[contains"
            + "(@data-cache-key,'undefined_false') "
            + "and not (contains(@style, 'display: none;'))]//div[@data-name='remove']"));
    private Element messageWithEmptySubject = new Element(By.xpath("//div[@class='b-datalist__item__subj' "
            + "and text()='<Без темы>']"));
    private Element messageWithEmptySubjectInInbox = new Element(By.xpath("//div[@data-cache-key='0_undefined_false']"
            + "//div[@class='b-datalist__item__subj' and text()='<Без темы>']"));
    private Element messageWithEmptyText = new Element(By.xpath("//span[@class='b-datalist__item__subj__snippet']"));
    private Element messageLetterSent = new Element(By.xpath("//div[@class='message-sent__title']"));

    public ListOfMailsPage deleteMessage() throws MalformedURLException, InterruptedException {
        deleteButton.click();
        return this;
    }

    public ListOfMailsPage markMessage(String letterSubject) throws MalformedURLException, InterruptedException {
        Browser.getInstance()
            .waitForAppear(LETTERS_CHECKBOX_PATTERN, letterSubject)
            .click(LETTERS_CHECKBOX_PATTERN, letterSubject);
        return this;
    }

    public boolean isEmptyMessageDisplayed() throws MalformedURLException, InterruptedException {
        return messageWithEmptySubject.isDisplayed() && messageWithEmptyText.isDisplayed();
    }

    public boolean isEmptyMessageDisplayedInInbox() throws MalformedURLException, InterruptedException {
        return messageWithEmptySubjectInInbox.isDisplayed() && messageWithEmptyText.isDisplayed();
    }

    public boolean isMessageDisplayedInInbox(String letterSubject, String letterText)
            throws MalformedURLException, InterruptedException {
        Browser.getInstance().waitForAppear(MESSAGE_SUBJECT_IN_INBOX_PATTERN, letterSubject);
        return Browser.getInstance().isDisplayed(MESSAGE_SUBJECT_IN_INBOX_PATTERN, letterSubject)
                && Browser.getInstance().isDisplayed(MESSAGE_TEXT_N_INBOX_PATTERN, letterText);
    }

    public boolean isMessageDisplayedInOutbox(String letterSubject, String letterText)
            throws MalformedURLException, InterruptedException {
        Browser.getInstance().waitForAppear(MESSAGE_SUBJECT_IN_OUTBOX_PATTERN, letterSubject);
        return Browser.getInstance().isDisplayed(MESSAGE_SUBJECT_IN_OUTBOX_PATTERN, letterSubject) && Browser.
                getInstance().isDisplayed(MESSAGE_TEXT_IN_OUTBOX_PATTERN, letterText);
    }

    public boolean isDeletedMessageDisplayed(String letterSubject, String letterText)
            throws MalformedURLException, InterruptedException {
        try {
            return Browser.getInstance().isDisplayed(MESSAGE_SUBJECT_IN_DELETED_PATTERN, letterSubject) && Browser
                    .getInstance().isDisplayed(MESSAGE_TEXT_IN_DELETED_PATTERN, letterText);
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public boolean isLetterSent() throws MalformedURLException, InterruptedException {
        return messageLetterSent.isDisplayed();
    }
}
