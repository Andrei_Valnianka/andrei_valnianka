package com.epam.tat.pages.cloud;

import com.epam.tat.framework.ui.Element;
import org.openqa.selenium.By;

import java.net.MalformedURLException;

public class CloudPublicPage {

    private final Element nameOfElement = new Element(By.xpath(
            "//span[@class='breadcrumbs__item__plain-header']"));

    public String getTitleElementText() throws MalformedURLException, InterruptedException {
        return nameOfElement.getText();
    }
}
