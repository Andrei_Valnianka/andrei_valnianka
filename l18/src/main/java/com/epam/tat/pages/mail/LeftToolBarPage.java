package com.epam.tat.pages.mail;

import com.epam.tat.framework.ui.Element;
import org.openqa.selenium.By;

import java.net.MalformedURLException;

public class LeftToolBarPage {

    private final Element writeLettersButton = new Element(By.xpath("//a[@data-name='compose']"));
    private final Element inboxLettersButton = new Element(By.xpath("//div[@data-id='0']"));
    private final Element outboxLettersButton = new Element(By.xpath("//div[@data-id='500000']"));
    private final Element deleteLettersButton = new Element(By.xpath("//div[@data-id='500002']"));
    private final Element draftLettersButton = new Element(By.xpath("//div[@data-id='500001']"));

    public LeftToolBarPage clickOnWriteLetterButton() throws MalformedURLException, InterruptedException {
        writeLettersButton.click();
        return this;
    }

    public LeftToolBarPage clickOnInboxLettersButton() throws MalformedURLException, InterruptedException {
        inboxLettersButton.click();
        return this;
    }

    public LeftToolBarPage clickOnOutboxLettersButton() throws MalformedURLException, InterruptedException {
        outboxLettersButton.click();
        return this;
    }

    public LeftToolBarPage clickOnDeletedLettersButton() throws MalformedURLException, InterruptedException {
        deleteLettersButton.click();
        return this;
    }

    public LeftToolBarPage clickOnDraftsLettersButton() throws MalformedURLException, InterruptedException {
        draftLettersButton.click();
        return this;
    }
}
