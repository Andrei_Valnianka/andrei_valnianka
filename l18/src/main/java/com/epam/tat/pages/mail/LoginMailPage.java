package com.epam.tat.pages.mail;

import com.epam.tat.framework.ui.Element;
import org.openqa.selenium.By;

import java.net.MalformedURLException;

public class LoginMailPage {

    private final Element invalidMessage = new Element(By.xpath("//div[@id='mailbox:authfail']"));
    private final Element loginForMail = new Element(By.xpath("//*[@id='PH_user-email']"));
    private final Element loginInput = new Element(By.xpath("//*[@id='mailbox__login']"));
    private final Element passwordInput = new Element(By.xpath("//*[@id='mailbox__password']"));
    private final Element submitButton = new Element(By.xpath("//*[@id='mailbox__auth__button']"));

    public LoginMailPage inputLogin(String email) throws MalformedURLException, InterruptedException {
        loginInput.sendKeys(email);
        return this;
    }

    public LoginMailPage inputPassword(String password) throws MalformedURLException, InterruptedException {
        passwordInput.sendKeys(password);
        return this;
    }

    public void clickSubmitButton() throws MalformedURLException, InterruptedException {
        submitButton.click();
    }

    public String getTextOfInvalidMessage() throws MalformedURLException, InterruptedException {
        return invalidMessage.getText();
    }

    public String getLoginName() throws MalformedURLException, InterruptedException {
        return loginForMail.getText();
    }
}
