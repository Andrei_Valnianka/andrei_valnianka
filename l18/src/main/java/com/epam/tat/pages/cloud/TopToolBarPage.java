package com.epam.tat.pages.cloud;

import com.epam.tat.framework.ui.Element;
import org.openqa.selenium.By;

import java.net.MalformedURLException;

public class TopToolBarPage {

    private final Element createButton = new Element(By.xpath(
            "//div[@id='toolbar-left']//div[@class='b-dropdown b-dropdown_create-document-dropdown']"));
    private final Element createFolder = new Element(By.xpath(
            "//div[@id='toolbar-left']//*[@data-name='folder']"));
    private final Element inputNameOfNewFolder = new Element(By.xpath("//*[@class='layer__input']"));
    private final Element submitNameOfNewFolder = new Element(By.xpath(
            "//div[@class='b-layer__controls__buttons']/*[@data-name='add']"));
    private final Element deleteButton = new Element(By.xpath(
            "//div[@data-name='remove' and not (contains (@aria-disabled,'disabled'))]"));
    private final Element removeConfirmationButton = new Element(By.xpath(
            "//div[@class='layer_remove']//button[@data-name='remove']"));
    private final Element addButton = new Element(By.xpath(
            "//*[@class='b-sticky' and not (contains(@style, 'hidden'))]//"
                    + "div[@data-name='upload' and not (contains(@class, 'drop-zone'))]"));
    private final Element moveConfirmationButton = new Element(By.xpath(
            "//div[@class='b-layer__container']//button[@data-name='move']"));
    private final Element moveConfirmationMessage = new Element(By.xpath(
            "//div[@class='layer_move-confirm']"));

    public TopToolBarPage clickOnCreateButton() throws MalformedURLException, InterruptedException {
        createButton.click();
        return this;
    }

    public TopToolBarPage clickOnCreateFolderButton() throws MalformedURLException, InterruptedException {
        createFolder.click();
        return this;
    }

    public TopToolBarPage enterNameOfNewFolder(String folderName) throws MalformedURLException, InterruptedException {
        inputNameOfNewFolder.sendKeys(folderName);
        return this;
    }

    public TopToolBarPage confirmNameOfNewFolder() throws MalformedURLException, InterruptedException {
        submitNameOfNewFolder.click();
        return this;
    }

    public TopToolBarPage clickOnDeleteButton() throws MalformedURLException, InterruptedException {
        deleteButton.click();
        return this;
    }

    public TopToolBarPage clickOnRemoveConfirmationButton() throws MalformedURLException, InterruptedException {
        removeConfirmationButton.click();
        return this;
    }

    public TopToolBarPage clickOnAddButton() throws MalformedURLException, InterruptedException {
        addButton.click();
        return this;
    }

    public TopToolBarPage clickMoveConfirmationButton() throws MalformedURLException, InterruptedException {
        moveConfirmationMessage.waitForAppear();
        moveConfirmationButton.click();
        return this;
    }
}
