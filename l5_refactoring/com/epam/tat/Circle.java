package com.epam.tat;

/**
 * Created by VZ-RED on 06.07.2017.
 */
public class Circle implements Shape{
    int radius;
    Circle (int radius) {
        this.radius = radius;
    }

    @Override
    public int area() {
        return (int) (3.14 * Math.pow(radius, 2));
    }
    @Override
    public int perimeter() {
        return (int) (2 * 3.14 * radius);
    }
}
