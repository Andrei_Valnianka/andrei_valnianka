package com.epam.tat;

import static java.lang.StrictMath.sqrt;

/**
 * Created by VZ-RED on 06.07.2017.
 */
public class Triangle implements Shape {

    int side;
    Triangle (int side) {
         this.side=side;
    }

    @Override
    public int area() {
        return (int) ((sqrt(3) * Math.pow(side, 2)) / 4);
    }
    @Override
    public int perimeter() {
        return 3 * side;
    }

}