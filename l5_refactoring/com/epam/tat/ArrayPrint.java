package com.epam.tat;

/**
 * Created by VZ-RED on 06.07.2017.
 */
public class ArrayPrint {

    public void trianglePrinter (int side, int area, int perimeter) {
        System.out.println("Triangle - " + side);
        System.out.println("         Area: " + area);
        System.out.println("         Perimeter: " + perimeter);
    }
    public void rectanglePrinter (int length,int width,int area, int perimeter){
        System.out.println("Rectangle - " + length+":"+width);
        System.out.println("         Area: " + area);
        System.out.println("         Perimeter: " + perimeter);
    }
    public void squarePrinter (int side, int area, int perimeter) {
        System.out.println("Square - " + side);
        System.out.println("         Area: " + area);
        System.out.println("         Perimeter: " + perimeter);
    }
    public void circlePrinter (int radius, int area, int perimeter) {
        System.out.println("Circle - " + radius);
        System.out.println("         Area: " + area);
        System.out.println("         Perimeter: " + perimeter);
    }
}
