package com.epam.tat;

import java.util.Scanner;

import static java.lang.System.in;
import static jdk.nashorn.internal.objects.NativeString.toUpperCase;

public class Main {

    public static void main(String[] args) {

        Object[] shapeCollection = new Object[args.length];
        ArrayPrint printer = new ArrayPrint();

        for (int i = 0; i < args.length; i++) {
            ShapesType shapesType = ShapesType.valueOf(toUpperCase(args[i].replaceAll("[^A-Za-z]", "")));
            int value;
            switch (shapesType ){
            case TRIANGLE: args[i] = args[i].replaceAll("\\D", "");
                value = Integer.parseInt(args[i]);
                Triangle triangle = new Triangle(value);
                shapeCollection[i] = triangle;
                printer.trianglePrinter(value, triangle.area(), triangle.perimeter());
                break;
            case RECTANGLE:String[] substringsForRectangle = args[i].split(":");
                int length = Integer.parseInt(substringsForRectangle[1]);
                int width = Integer.parseInt(substringsForRectangle[2]);
                Rectangle rectangle = new Rectangle(length, width);
                shapeCollection[i] = rectangle;
                printer.rectanglePrinter(length, width, rectangle.area(), rectangle.perimeter());
                break;
            case SQUARE:args[i] = args[i].replaceAll("\\D", "");
                value = Integer.parseInt(args[i]);
                Square square = new Square(value);
                shapeCollection[i] = square;
                printer.squarePrinter(value, square.area(), square.perimeter());
                break;
            case CIRCLE:args[i] = args[i].replaceAll("\\D", "");
                value = Integer.parseInt(args[i]);
                Circle circle = new Circle(value);
                shapeCollection[i] = circle;
                printer.circlePrinter(value, circle.area(), circle.perimeter());
                break;
            }
        }
        shapeParser();
    }
    public static void shapeParser() {

        System.out.println("Enter string for parse");
        Scanner scanner = new Scanner(in);
        String stringForParse = scanner.nextLine();

        ShapesType shapeType = ShapesType.valueOf(toUpperCase(stringForParse.replaceAll("[^A-Za-z]", "")));
                switch (shapeType){
                case TRIANGLE: System.out.println("TRIANGLE");
                    break;
                case RECTANGLE: System.out.println("RECTANGLE");
                     break;
                case SQUARE: System.out.println("SQUARE");
                     break;
                case CIRCLE: System.out.println("CIRCLE");
                     break;
            }
    }

}
