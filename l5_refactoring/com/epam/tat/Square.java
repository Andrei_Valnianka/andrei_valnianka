package com.epam.tat;

/**
 * Created by VZ-RED on 06.07.2017.
 */
public class Square implements Shape{
    int side;
    Square (int side) {
        this.side=side;
    }

    @Override
    public int area() {
        return side * side;
    }
    @Override
    public int perimeter() {
        return 4 * side;
    }
}
