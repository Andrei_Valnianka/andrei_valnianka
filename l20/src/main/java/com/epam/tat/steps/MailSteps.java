package com.epam.tat.steps;

import com.epam.tat.framework.bo.Account;
import com.epam.tat.framework.bo.AccountFactory;
import com.epam.tat.framework.bo.Letter;
import com.epam.tat.framework.bo.LetterFactory;
import com.epam.tat.services.AuthenticationService;
import com.epam.tat.services.MailService;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;
import org.testng.Assert;

public class MailSteps extends Steps {
    private static final String INCORRECT_ADDRESS_MESSAGE = "В поле «Кому» указан некорректный адрес получателя.\n"
            + "Исправьте ошибку и отправьте письмо ещё раз.";
    private Account correctAccount = new AccountFactory().getCorrectAccount();
    private AuthenticationService authenticationService = new AuthenticationService();
    private MailService mailService = new MailService();
    private Letter letterWithEmailSubjectBody;
    private Letter letterWithoutSubjectAndBody;
    private Letter letterForDraftMailTest;

    @Given("Correct login")
    @When("Correct login")
    public void login() {
        authenticationService.loginMail(correctAccount.getLogin(), correctAccount.getPassword());
    }

    @When("I send letter with email subject body")
    public void sendLetterWithEmailSubjectBody() {
        letterWithEmailSubjectBody = new LetterFactory().getLetterWithEmailSubjectBody();
        mailService.sendLetter(letterWithEmailSubjectBody);
    }

    @Then("I see letter with email subject body in inbox and outbox")
    public void isLetterPresentInInboxAndOutbox() {
        Assert.assertEquals(mailService
                .isLetterWithSubjectAndBodyDisplayedInInboxAndOutbox(letterWithEmailSubjectBody), "");
    }

    @When("I send letter with invalid address")
    public void sendLetterWithInvalidAddress() {
        mailService.sendInvalidLetter(new LetterFactory().getLetterWithInvalidAddress());
    }

    @Then("I see invalid letter message")
    public void isInvalidMessageDisplayed() {
        Assert.assertEquals(mailService.getTextOfInvalidMessage(), INCORRECT_ADDRESS_MESSAGE);
    }

    @When("I send letter without subject and body")
    public void sendLetterWithoutSubjectBody() {
        letterWithoutSubjectAndBody = new LetterFactory().getLetterWithoutSubjectAndBody();
        mailService.sendLetterWithoutSubjectAndBody(letterWithoutSubjectAndBody);
    }

    @Then("I see empty letter with email subject body in inbox and outbox")
    public void isEmptyLetterDisplayedInboxAndOutbox() {
        Assert.assertEquals(mailService.isEmptyLetterDisplayedInInboxAndOutbox(), "");
    }

    @When("I create letter")
    public void createLetterDraft() {
        letterForDraftMailTest = new LetterFactory().getLetterWithEmailSubjectBody();
    }

    @When("I save letter in draft")
    public void saveLetterDraft() {
        mailService.saveLetterInDraft(letterForDraftMailTest);
    }

    @When("I delete letter in draft")
    public void deleteLetterDraft() {
        mailService.deleteLetterInDraft(letterForDraftMailTest);
    }

    @When("I delete letter in deleted")
    public void deleteLetterDeleted() {
        mailService.deleteLetterInDeleted(letterForDraftMailTest);
    }

    @Then("I don't see letter in deleted and draft")
    public void isDeletedLetterDisplayedDraftDeleted() {
        Assert.assertEquals(mailService.isDeletedLetterDisplayed(letterForDraftMailTest), "");
    }

    @Then("I see login name")
    public void isMailLoginNameDisplayed() {
        Assert.assertEquals(authenticationService.getMailLoginName(), correctAccount.getLogin());
    }

    @When("I set $login and $password")
    public void setLoginPassword(@Named("login") String login, @Named("password") String password) {
        authenticationService.loginMail(login, password);
    }

    @Then("I see invalid login $message")
    public void isInvalidLoginMessageDisplayed(@Named("message") String expectedResult) {
        Assert.assertEquals(authenticationService.getTextOfInvalidMailMessage(), expectedResult);
    }
}
