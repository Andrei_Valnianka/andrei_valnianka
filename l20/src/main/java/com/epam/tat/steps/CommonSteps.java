package com.epam.tat.steps;

import com.epam.tat.framework.ui.Browser;
import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.annotations.ScenarioType;
import org.jbehave.core.steps.Steps;

public class CommonSteps extends Steps {

    @BeforeScenario(uponType = ScenarioType.ANY)
    public void setUpDriver() {
        Browser.getInstance().getUrlAndMaximize("http://Mail.ru");
    }

    @AfterScenario
    public void tearDown() {
        Browser.getInstance().stopBrowser();
    }
}
