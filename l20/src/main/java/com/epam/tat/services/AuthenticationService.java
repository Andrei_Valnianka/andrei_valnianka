package com.epam.tat.services;

import com.epam.tat.framework.logging.Log;
import com.epam.tat.pages.mail.LoginMailPage;

public class AuthenticationService {

    public void loginMail(String email, String password) {
        Log.info(String.format("Login to Mail.ru [%s; %s]", email, password));
        LoginMailPage loginMailPage = new LoginMailPage();
        loginMailPage.inputLogin(email)
                .inputPassword(password)
                .clickSubmitButton();
    }

    public String getMailLoginName() {
        Log.info("Get login name");
        return new LoginMailPage().getLoginName();
    }

    public String getTextOfInvalidMailMessage() {
        Log.info("Get text of invalid Mail message");
        return new LoginMailPage().getTextOfInvalidMessage();
    }
}
