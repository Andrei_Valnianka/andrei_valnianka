package com.epam.tat.framework.runner;

import com.epam.tat.steps.CommonSteps;
import com.epam.tat.steps.MailSteps;
import org.jbehave.core.InjectableEmbedder;
import org.jbehave.core.annotations.Configure;
import org.jbehave.core.annotations.UsingEmbedder;
import org.jbehave.core.annotations.UsingSteps;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.embedder.StoryControls;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.junit.AnnotatedEmbedderRunner;
import org.jbehave.core.parsers.RegexPrefixCapturingPatternParser;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.ParameterConverters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.text.SimpleDateFormat;
import java.util.List;

import static com.epam.tat.framework.runner.StoriesRunner.STORY_TIMEOUT;
import static org.jbehave.core.io.CodeLocations.codeLocationFromClass;
import static org.jbehave.core.reporters.StoryReporterBuilder.Format.*;

@RunWith(AnnotatedEmbedderRunner.class)
@Configure(storyControls = StoriesRunner.MyStoryControls.class,
        storyLoader = StoriesRunner.MyStoryLoader.class, storyReporterBuilder = StoriesRunner.MyReportBuilder.class,
        parameterConverters = { StoriesRunner.MyDateConverter.class })
@UsingEmbedder(embedder = Embedder.class, generateViewAfterStories = true,
        ignoreFailureInStories = false, ignoreFailureInView = false, verboseFailures = true,
        storyTimeoutInSecs = STORY_TIMEOUT, threads = 1, metaFilters = "-skip")
@UsingSteps(instances = { CommonSteps.class, MailSteps.class})
public class StoriesRunner extends InjectableEmbedder {
    public static final long STORY_TIMEOUT = 500;

    @Test
    public void run() {
        List<String> storyPaths = new StoryFinder().findPaths(codeLocationFromClass(this.getClass()), "**/*.story", "");
        injectedEmbedder().runStoriesAsPaths(storyPaths);
    }

    public static class MyStoryControls extends StoryControls {
        public MyStoryControls() {
            doDryRun(false);
            doSkipScenariosAfterFailure(false);
        }
    }

    public static class MyStoryLoader extends LoadFromClasspath {
        public MyStoryLoader() {
            super(StoriesRunner.class.getClassLoader());
        }
    }

    public static class MyReportBuilder extends StoryReporterBuilder {
        public MyReportBuilder() {
            this.withFormats(CONSOLE, TXT, HTML, XML).withDefaultFormats();
        }
    }

    public static class MyRegexPrefixCapturingPatternParser extends RegexPrefixCapturingPatternParser {
        public MyRegexPrefixCapturingPatternParser() {
            super("%");
        }
    }

    public static class MyDateConverter extends ParameterConverters.DateConverter {
        public MyDateConverter() {
            super(new SimpleDateFormat("dd-MM-yyyy"));
        }
    }
}
