package com.epam.tat.framework.logging;

import org.apache.log4j.Logger;

public class Log {

    private static Logger log = Logger.getLogger("com.epam.tat");

    public static void info(Object message) {
        log.info(message);
    }

    public static void error(Object message) {
        log.error(message);
    }

    public static void error(Object message, Throwable e) {
        log.error(message, e);
    }

    public static void debug(Object message) {
        log.debug(message);
    }

}
