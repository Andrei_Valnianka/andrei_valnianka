Feature: Check login to mail.ru.

Narrative:
As a user
I want login to mail.ru with valid and invalid login and password
So that I can check the mail.ru authentication service

Scenario: Correct login test
When Correct login
Then I see login name

Scenario: Negative login test
When: I set <login> and <password>
Then: I see invalid login <message>

Examples:
|login|password|message|
|""|"ekscentrisitet2018"|"Введите имя ящика"|
|""|""|"Введите имя ящика"|
|"vasil_pupkin2018@mail.ru"|""|"Введите пароль"|
