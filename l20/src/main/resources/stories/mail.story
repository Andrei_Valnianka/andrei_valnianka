Feature: Check mail.ru.

Narrative:
As a valid user
I want to do CRUD operation in mail.ru
So that I can check the mail.ru main functionality

Scenario: Create draft letter and delete it
Given Correct login
When I create letter
And I save letter in draft
And I delete letter in draft
And I delete letter in deleted
Then I don't see letter in deleted and draft

Scenario: Send letter without subject and body
Given Correct login
When I send letter without subject and body
Then I see empty letter with email subject body in inbox and outbox

Scenario: Send letter with subject and body
Given Correct login
When I send letter with email subject body
Then I see letter with email subject body in inbox and outbox

Scenario: Send letter with invalid email
Given Correct login
When I send letter with invalid address
Then I see invalid letter message
