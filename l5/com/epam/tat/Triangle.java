package com.epam.tat;

import static java.lang.StrictMath.sqrt;

/**
 * Created by VZ-RED on 05.07.2017.
 */
public class Triangle extends ShapeImpl {

    private int side;

    public int area(int value1, int value2) {
        return (int) ((sqrt(3) * Math.pow(value1, 2)) / 4);
    }

    public int perimeter(int value1, int value2) {
        return 3 * value1;
    }

    void setValues(int value1, int value2) {
        side = value1;
    }

    int getValue1() {
        return side;
    }

    int getValue2() {
        return 0;
    }

    String getName() {
        return "Triangle";
    }
}
