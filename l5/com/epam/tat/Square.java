package com.epam.tat;

/**
 * Created by VZ-RED on 05.07.2017.
 */
public class Square extends ShapeImpl {

    private int side;

    public int area(int value1, int value2) {
        return value1 * value1;
    }

    public int perimeter(int value1, int value2) {
        return value1 * 4;
    }

    void setValues(int value1, int value2) {
        side = value1;
    }

    int getValue1() {
        return side;
    }

    int getValue2() {
        return 0;
    }

    String getName() {
        return "Square";
    }
}
