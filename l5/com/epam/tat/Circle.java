package com.epam.tat;

/**
 * Created by VZ-RED on 05.07.2017.
 */
public class Circle extends ShapeImpl {

    private int radius;

    public int area(int value1, int value2) {
        return (int) (3.14 * Math.pow(value1, 2));
    }

    public int perimeter(int value1, int value2) {
        return (int) (2 * 3.14 * value1);
    }

    void setValues(int value1, int value2) {
        radius = value1;
    }

    int getValue1() {
        return radius;
    }

    int getValue2() {
        return 0;
    }

    String getName() {
        return "Circle";
    }
}
