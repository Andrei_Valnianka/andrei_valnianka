package com.epam.tat;

/**
 * Created by VZ-RED on 05.07.2017.
 */
public class Rectangle extends ShapeImpl {

    private int length;
    private int width;

    public int area(int length, int width) {
        return length * width;
    }

    public int perimeter(int length, int width) {
        return 2 * length + 2 * width;
    }

    void setValues(int value1, int value2) {
        length = value1;
        width = value2;
    }

    int getValue1() {
        return length;
    }

    int getValue2() {
        return width;
    }

    String getName() {
        return "Rectangle";
    }
}
