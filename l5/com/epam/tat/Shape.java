package com.epam.tat;

/**
 * Created by VZ-RED on 05.07.2017.
 */
public interface Shape {

    int area(int value1, int value2);

    int perimeter(int value1, int value2);
}
