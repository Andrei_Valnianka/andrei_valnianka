package com.epam.tat;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.System.in;

public class Main {

    enum ShapesType {CIRCLE, TRIANGLE, SQUARE, RECTANGLE}

    public static void main(String[] args) {

        ShapeImpl shapeImpl = new ShapeImpl();
        ShapeImpl[] shapeCollection = new ShapeImpl[args.length];

        for (int i = 0; i < args.length; i++) {
            Pattern p1 = Pattern.compile("^(C|c)ircle:\\d+$");
            Pattern p2 = Pattern.compile("^(T|t)riangle:\\d+$");
            Pattern p3 = Pattern.compile("^(S|s)quare:\\d+$");
            Pattern p4 = Pattern.compile("^(R|r)ectangle:\\d+:\\d+$");
            Matcher m1 = p1.matcher(args[i]);
            Matcher m2 = p2.matcher(args[i]);
            Matcher m3 = p3.matcher(args[i]);
            Matcher m4 = p4.matcher(args[i]);

            if (m1.matches()) {
                args[i] = args[i].replaceAll("\\D", "");
                int value = Integer.parseInt(args[i]);
                shapeCollection[i] = new Circle();
                shapeCollection[i].setValues(value, 0);
            }
            if (m2.matches()) {
                args[i] = args[i].replaceAll("\\D", "");
                int value = Integer.parseInt(args[i]);
                shapeCollection[i] = new Triangle();
                shapeCollection[i].setValues(value, 0);
            }
            if (m3.matches()) {
                args[i] = args[i].replaceAll("\\D", "");
                int value = Integer.parseInt(args[i]);
                shapeCollection[i] = new Square();
                shapeCollection[i].setValues(value, 0);
            }
            if (m4.matches()) {
                String[] substringsForRectangle = args[i].split(":");
                int value1 = Integer.parseInt(substringsForRectangle[1]);
                int value2 = Integer.parseInt(substringsForRectangle[2]);
                shapeCollection[i] = new Rectangle();
                shapeCollection[i].setValues(value1, value2);
            }
        }

        for (int i = 0; i < args.length; i++) {
            System.out.println(shapeCollection[i].getName() + " - " + shapeCollection[i].getValue1());
            if (shapeCollection[i] instanceof Rectangle) {
                System.out.println(":" + shapeCollection[i].getValue2());
            }
            System.out.println("         Area: " + shapeCollection[i].area(shapeCollection[i].getValue1(), shapeCollection[i].getValue2()));
            System.out.println("         Perimeter: " + shapeCollection[i].perimeter(shapeCollection[i].getValue1(), shapeCollection[i].getValue2()));
        }
        shapeParser();
    }

    public static void shapeParser() {

        System.out.println("Enter string for parse");
        Scanner scanner = new Scanner(in);
        String stringForParse = scanner.nextLine();

        Pattern p1 = Pattern.compile("^(C|c)ircle:\\d+$");
        Pattern p2 = Pattern.compile("^(T|t)riangle:\\d+$");
        Pattern p3 = Pattern.compile("^(S|s)quare:\\d+$");
        Pattern p4 = Pattern.compile("^(R|r)ectangle:\\d+:\\d+$");
        Matcher m1 = p1.matcher(stringForParse);
        Matcher m2 = p2.matcher(stringForParse);
        Matcher m3 = p3.matcher(stringForParse);
        Matcher m4 = p4.matcher(stringForParse);

        if (m1.matches()) {
            String name = "CIRCLE";
            ShapesType shapesType = ShapesType.valueOf(name);
            System.out.println(shapesType);
        }
        if (m2.matches()) {
            String name = "TRIANGLE";
            ShapesType shapesType = ShapesType.valueOf(name);
            System.out.println(shapesType);
        }
        if (m3.matches()) {
            String name = "SQUARE";
            ShapesType shapesType = ShapesType.valueOf(name);
            System.out.println(shapesType);
        }
        if (m4.matches()) {
            String name = "RECTANGLE";
            ShapesType shapesType = ShapesType.valueOf(name);
            System.out.println(shapesType);
        }
    }
}
